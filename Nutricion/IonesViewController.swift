//
//  IonesViewController.swift
//  Nutricion
//
//  Created by Julio Barado Hualde on 04/04/2019.
//  Copyright © 2019 Julio Barado Hualde. All rights reserved.
//

import UIKit

class IonesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {
   
    @IBOutlet weak var tabla: UITableView!
    @IBOutlet weak var marcoPickerView: UIView!
    @IBOutlet var NutricionIonesView: UIView!

    var secciones = ["Iones", "Volumen"]
    
    var etiquetas = [["Sodio", "Cloruro sódico", "Acetato sódico", "Potasio", "Cloruro potásico", "Acetato potásico", "Fósforo", "Fosfato monopotásico", "Fosfato dipotásico", "Cloro", "Calcio, gluconato","Magnesio, sulfato"],["Glutamina", "Polivitamínico", "Oligoelementos", "Volumen H de C (mL)", "Volumen Lípidos (mL)", "Volumen Proteinas (mL)", "Volumen Iones (mL)", "Volumen mínimo (mL)"]]

    var valores = [["Sodio", "Cloruro sódico", "Acetato sódico", "Potasio", "Cloruro potásico", "Acetato potásico", "Fósforo", "Fosfato monopotásico", "Fosfato dipotásico", "Cloro", "Calcio, gluconato", "Magnesio, sulfato"],["", "", "", "Volumen H de C (mL)", "Volumen Lípidos (mL)", "Volumen Proteinas (mL)", "Volumen Iones (mL)", "Volumen mínimo (mL)"]]

    
    var picker01 = UIPickerView()
    var tipoPicker: Int = 1
    var indiceActual: IndexPath = []
    var marcadorFila = 0
    var posicionesGuardadas: [[Int]] = []
    var posicionesGuardadasAntiguas: [[Int]] = []

    var valoresActuales: [Double] = []
    var valoresAntiguos: [Double] = []
    var abrirPrimeraVez = true
    
    @IBOutlet weak var alturaConstraint: NSLayoutConstraint!
    @IBOutlet weak var pgControlVeriticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var pgControlCentralConstraint: NSLayoutConstraint!
    @IBOutlet weak var barraCloseView: UIView!

    @IBAction func cerrarVentanaButton(_ sender: UIButton?) {
        
        if menuShowing {
            alturaConstraint.constant = 124
            pgControlCentralConstraint.constant = 0
            pgControlVeriticalConstraint.constant = -134
            
        }
        else{
            alturaConstraint.constant = 0
            pgControlCentralConstraint.constant = 145
            pgControlVeriticalConstraint.constant = -10
        }
        menuShowing = !menuShowing
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn,animations: {self.view.layoutIfNeeded()})
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let camasOut = UserDefaults.standard.data(forKey: "ListadoCamas")
        let lista = try! JSONDecoder().decode([Nutricion].self, from: camasOut!)
        listadoCamas = lista

        
        picker01.delegate = self
        picker01.dataSource = self

        marcadorFila = 99 // Se usa un valor que no existe para que no se elimine ninguna variable de asignar índices en el siguiente paso.
        actualizarCheckmarkers()
        asignarIndices()
        marcadorFila = 0

        
        
        mostrarPicker()
        cerrarVentanaAlCargarVistas()
        print("Menushowing download iones:\(menuShowing)")
        
        let indexPath = IndexPath(row: 0, section: 0)
        tabla.selectRow(at: indexPath, animated: true, scrollPosition: .top)
        self.tableView(self.tabla, didSelectRowAt: indexPath) // se invoca a la accion didseletrow de la tabla
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(ocultar(gesto:)))
        barraCloseView.addGestureRecognizer(tap)

        
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(ocult(gesto:)))
        marcoPickerView.addGestureRecognizer(pinch)

        let inferior = UISwipeGestureRecognizer(target: self, action: #selector(ocult(gesto:)))
        inferior.direction = .right
        marcoPickerView.addGestureRecognizer(inferior)
        
        let swipeInferior = UISwipeGestureRecognizer(target: self, action: #selector(ocultarVentanaIones(gesto:)))
        swipeInferior.direction = UISwipeGestureRecognizer.Direction.down
        barraCloseView.addGestureRecognizer(swipeInferior)
        
        let swipeSuperior = UISwipeGestureRecognizer(target: self, action: #selector(ocultarVentanaIones(gesto:)))
        swipeSuperior.direction = UISwipeGestureRecognizer.Direction.up
        barraCloseView.addGestureRecognizer(swipeSuperior)
       
        
        
}
    

    @objc func ocultar(gesto: UISwipeGestureRecognizer){
        print("Tap")
    }
    @objc func ocult(gesto: UISwipeGestureRecognizer){
        print("Pinch")
    }
    
    @objc func ocultarVentanaIones(gesto: UISwipeGestureRecognizer){
        print("Gesta")
        if let swipeGesto = gesto as? UISwipeGestureRecognizer{
            print("Gesto")
            switch swipeGesto.direction {
            case UISwipeGestureRecognizer.Direction.down:
                alturaConstraint.constant = 0
                pgControlCentralConstraint.constant = 145
                pgControlVeriticalConstraint.constant = -10
            case UISwipeGestureRecognizer.Direction.up:
                alturaConstraint.constant = 124
                pgControlCentralConstraint.constant = 0
                pgControlVeriticalConstraint.constant = -134
            default:
                break
            }
            menuShowing = !menuShowing
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn,animations: {self.view.layoutIfNeeded()})
        }
    }
    
    func cerrarVentanaAlCargarVistas(){
        if menuShowing {
            alturaConstraint.constant = 0
            pgControlCentralConstraint.constant = 145
            pgControlVeriticalConstraint.constant = -10
        }
        else{
            alturaConstraint.constant = 124
            pgControlCentralConstraint.constant = 0
            pgControlVeriticalConstraint.constant = -134

        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //tabla.reloadData()
        actualizarCheckmarkers()
        mostrarPicker()
        cerrarVentanaAlCargarVistas()
 
        print("Menushowing appear iones:\(menuShowing)")
    }
    
    func actualizarCheckmarkers(){
        if listadoCamas[Int(cama)].valoresActualesIones[16] > 0{
            listadoCamas[Int(cama)].checked[0] = true
        }
        else {
            listadoCamas[Int(cama)].checked[0] = false
        }
        if listadoCamas[Int(cama)].valoresActualesIones[17] > 0{
            listadoCamas[Int(cama)].checked[1] = true
        }
        else {
            listadoCamas[Int(cama)].checked[1] = false
        }
        if listadoCamas[Int(cama)].valoresActualesIones[18] > 0{
            listadoCamas[Int(cama)].checked[2] = true
        }
        else {
            listadoCamas[Int(cama)].checked[2] = false
        }
    }
    
    
    func selectRow(tableView: UITableView, position: Int) {
        let sizeTable = tableView.numberOfRows(inSection: 1)
        guard position >= 0 && position < sizeTable else { return }
        let indexPath = IndexPath(row: position, section: 1)
        
        
        tableView.selectRow(at: indexPath, animated: true, scrollPosition: .middle)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return secciones.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return secciones[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return etiquetas[section].count
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        header.textLabel?.textColor = .blue
        header.textLabel?.textAlignment = NSTextAlignment.center
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath == indiceActual {
            switch (indexPath.section, indexPath.row)  {
                case (0, 0), (0, 3), (0, 6), (0, 9), (0, 10), (0, 11):
                    let celda = tableView.dequeueReusableCell(withIdentifier: "MiCeldaSeleccionadaIones", for: indexPath) as! CeldaSeleccionadaIones
                    celda.etiquetas.text = etiquetas[indexPath.section][indexPath.row]
                    celda.valores.text = valores[indexPath.section][indexPath.row]
                    return celda
            case (1, 0), (1, 1), (1, 2):
                let celda = tableView.dequeueReusableCell(withIdentifier: "MiCeldaSwitch", for: indexPath) as! CeldaSwitch
                celda.etiquetas.text = etiquetas[indexPath.section][indexPath.row]
          
                
                switch (indexPath.section, indexPath.row)  {
                    case(1, 0):

                        if listadoCamas[Int(cama)].checked[0] == false{
                            celda.accessoryType = .none
                        }
                        else {
                            celda.accessoryType = .checkmark
                        }
                        print("Elección: \(listadoCamas[Int(cama)].checked[0])")
                    case(1, 1):
                        if listadoCamas[Int(cama)].checked[1] == false{
                            celda.accessoryType = .none
                        }
                        else {
                            celda.accessoryType = .checkmark
                        }
                    default:
                        if listadoCamas[Int(cama)].checked[2] == false{
                            celda.accessoryType = .none
                        }
                        else {
                            celda.accessoryType = .checkmark
                        }
                }
 
                return celda
 
            case (1, 3), (1, 4), (1, 5), (1, 6):
                let celda = tableView.dequeueReusableCell(withIdentifier: "MiCelda", for: indexPath) as! CeldasView
                celda.etiquetas.text = etiquetas[indexPath.section][indexPath.row]
                celda.valores.text = valores[indexPath.section][indexPath.row]
                return celda
            case (1, 7):
                let celda = tableView.dequeueReusableCell(withIdentifier: "MiCeldaTitulo", for: indexPath) as! CeldaTitulo
                celda.etiquetas.text = etiquetas[indexPath.section][indexPath.row]
                celda.valores.text = valores[indexPath.section][indexPath.row]
                return celda
            default:
                let celda = tableView.dequeueReusableCell(withIdentifier: "MiCeldaSeleccionada", for: indexPath) as! CeldaSeleccionada
                celda.etiquetas.text = etiquetas[indexPath.section][indexPath.row]
                celda.valores.text = valores[indexPath.section][indexPath.row]
                return celda
                }
        
        }
        else {
            switch (indexPath.section, indexPath.row)  {
                case (0, 0), (0, 3), (0, 6), (0, 9), (0, 10), (0, 11):
                    let celda = tableView.dequeueReusableCell(withIdentifier: "MiCeldaTitulo", for: indexPath) as! CeldaTitulo
                    celda.etiquetas.text = etiquetas[indexPath.section][indexPath.row]
                    celda.valores.text = valores[indexPath.section][indexPath.row]
                    return celda
                
                case (1, 0), (1, 1), (1, 2):
                    let celda = tableView.dequeueReusableCell(withIdentifier: "MiCeldaSwitch", for: indexPath) as! CeldaSwitch
                    celda.etiquetas.text = etiquetas[indexPath.section][indexPath.row]
                    celda.accessoryType = .checkmark
                    switch (indexPath.section, indexPath.row)  {
                        case(1, 0):
                            if listadoCamas[Int(cama)].checked[0] == false{
                                celda.accessoryType = .none
                            }
                            else {
                                celda.accessoryType = .checkmark
                            }
                        case(1, 1):
                            if listadoCamas[Int(cama)].checked[1] == false{
                                celda.accessoryType = .none
                            }
                            else {
                                celda.accessoryType = .checkmark
                            }
                        default:
                            if listadoCamas[Int(cama)].checked[2] == false{
                                celda.accessoryType = .none
                            }
                            else {
                                celda.accessoryType = .checkmark
                            }
                    }
                    return celda
            case (1, 3), (1, 4), (1, 5), (1, 6):
                let celda = tableView.dequeueReusableCell(withIdentifier: "MiCelda", for: indexPath) as! CeldasView
                celda.etiquetas.text = etiquetas[indexPath.section][indexPath.row]
                celda.valores.text = valores[indexPath.section][indexPath.row]
                return celda
            case (1, 7):
                let celda = tableView.dequeueReusableCell(withIdentifier: "MiCeldaTitulo", for: indexPath) as! CeldaTitulo
                celda.etiquetas.text = etiquetas[indexPath.section][indexPath.row]
                celda.valores.text = valores[indexPath.section][indexPath.row]
                return celda
            default:
                let celda = tableView.dequeueReusableCell(withIdentifier: "MiCelda", for: indexPath) as! CeldasView
                celda.etiquetas.text = etiquetas[indexPath.section][indexPath.row]
                celda.valores.text = valores[indexPath.section][indexPath.row]
                return celda
            }
        }
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        alturaConstraint.constant = 124
        pgControlCentralConstraint.constant = 0
        pgControlVeriticalConstraint.constant = -134
        
        if abrirPrimeraVez {
            menuShowing == menuShowing
            abrirPrimeraVez = false
        }
        else {
            menuShowing = true
        }
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn,animations: {self.view.layoutIfNeeded()})
        
        picker01.isUserInteractionEnabled = true
        picker01.alpha = 1
        indiceActual = tabla.indexPathForSelectedRow ?? [0,0]
        tabla.reloadData()
        
        switch (indexPath.section, indexPath.row){
        case (0, 0):
            //Sodio
            marcadorFila = 0
            
        case (0, 1):
            //Cloruro sódico
            marcadorFila = 1
            
        case (0, 2):
            //Acetato sódico
            marcadorFila = 2
            
        case (0, 3):
            //Potasio
            marcadorFila = 3
            
        case (0, 4):
            //Cloruro potásico
            marcadorFila = 4
            
        case (0, 5):
            //Acetato potásico
            marcadorFila = 5
            
        case (0, 6):
            //Fósforo
            marcadorFila = 6
            
        case (0, 7):
            //Fosfato monopotásico
            marcadorFila = 7
            
        case (0, 8):
            //fosfato dipotasico
            marcadorFila = 8
            
        case (0, 9):
            //Cloro
            marcadorFila = 9
            
        case (0, 10):
            //Calcio
            marcadorFila = 10
        
        case (0, 11):
            //Magnesio
            marcadorFila = 11
        case (1, 0):
            //Glutamina
            marcadorFila = 12
            picker01.isUserInteractionEnabled = false
            picker01.alpha = 0.5
            menuShowing = false
            cerrarVentanaButton(nil)
            
            if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
                if cell.accessoryType == .checkmark {
                    cell.accessoryType = .none
                    listadoCamas[Int(cama)].valoresActualesIones[16] = 0
                    listadoCamas[Int(cama)].checked[0] = false
                } else {
                    cell.accessoryType = .checkmark
                    listadoCamas[Int(cama)].valoresActualesIones[16] = 5
                    listadoCamas[Int(cama)].checked[0] = true
                }
            }

         print("Glutamina: \(listadoCamas[Int(cama)].valoresActualesIones[16])")
        case (1, 1):
            //Polivitamínico
            marcadorFila = 13
            picker01.isUserInteractionEnabled = false
            picker01.alpha = 0.5
            menuShowing = false
            cerrarVentanaButton(nil)
            if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
                if cell.accessoryType == .checkmark {
                    cell.accessoryType = .none
                    listadoCamas[Int(cama)].valoresActualesIones[17] = 0
                    listadoCamas[Int(cama)].checked[1] = false
                } else {
                    cell.accessoryType = .checkmark
                    listadoCamas[Int(cama)].valoresActualesIones[17] = 10
                    listadoCamas[Int(cama)].checked[1] = true
                }
            }


        case (1, 2):
            //Oligoelementos
            marcadorFila = 14
            picker01.isUserInteractionEnabled = false
            picker01.alpha = 0.5
            menuShowing = false
            cerrarVentanaButton(nil)
            if let cell = tableView.cellForRow(at: indexPath as IndexPath) {
                if cell.accessoryType == .checkmark {
                    cell.accessoryType = .none
                    listadoCamas[Int(cama)].valoresActualesIones[18] = 0
                    listadoCamas[Int(cama)].checked[2] = false
                } else {
                    cell.accessoryType = .checkmark
                    listadoCamas[Int(cama)].valoresActualesIones[18] = 15
                    listadoCamas[Int(cama)].checked[2] = true
                }
            }

        case (1, 3):
            //Volumen HdeC
            marcadorFila = 15
            picker01.isUserInteractionEnabled = false
            picker01.alpha = 0.5
            menuShowing = false
            cerrarVentanaButton(nil)
        case (1, 4):
            //Volumen Lípidos
            marcadorFila = 16
            picker01.isUserInteractionEnabled = false
            picker01.alpha = 0.5
            menuShowing = false
            cerrarVentanaButton(nil)
        case (1, 5):
            //Volumen Proteinas
            marcadorFila = 17
            picker01.isUserInteractionEnabled = false
            picker01.alpha = 0.5
            menuShowing = false
            cerrarVentanaButton(nil)
        case (1, 6):
            //Volumen iones
            marcadorFila = 18
            picker01.isUserInteractionEnabled = false
            picker01.alpha = 0.5
            menuShowing = false
            cerrarVentanaButton(nil)
        case (1, 7):
            //Volumen mínimo
            marcadorFila = 19
            picker01.isUserInteractionEnabled = false
            picker01.alpha = 0.5
            menuShowing = false
            cerrarVentanaButton(nil)
        default:
            break
        }
        picker01.reloadAllComponents()
        
        mostrarPicker()
        print("Menushowing download después didselect:\(menuShowing)")


}
    func mostrarPicker(){
        self.view.addSubview(picker01)
        picker01.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addConstraint(
            NSLayoutConstraint (item: picker01,
                                attribute: .width,
                                relatedBy: .equal,
                                toItem: marcoPickerView,
                                attribute: .width,
                                multiplier: 0.9,
                                constant: 0))
        
        self.view.addConstraint(
            NSLayoutConstraint (item: picker01,
                                attribute: .height,
                                relatedBy: .equal,
                                toItem: marcoPickerView,
                                attribute: .height,
                                multiplier: 0.85,
                                constant: 0))
        
        self.view.addConstraint(
            NSLayoutConstraint (item: picker01,
                                attribute: .centerX,
                                relatedBy: .equal,
                                toItem: marcoPickerView,
                                attribute: .centerX,
                                multiplier: 1,
                                constant: 0))
        
        self.view.addConstraint(
            NSLayoutConstraint (item: picker01,
                                attribute: .centerY,
                                relatedBy: .equal,
                                toItem: marcoPickerView,
                                attribute: .centerY,
                                multiplier: 1,
                                constant: 0))
        situarPicker()
    }
    
    func situarPicker(){
        // Situación de los componentes del picker según las posiciones guardadas
        var inicio: Int = 0
        var fin: Int = 0
        switch marcadorFila {
        case 0...5, 9:
            inicio = 1
            fin = 3
        case 6, 7, 8, 10, 11:
            inicio = 2
            fin = 4

        default:
            break
        }
       
        
        for i in inicio...fin {
            picker01.selectRow(listadoCamas[Int(cama)].posicionesGuardadasActualesIones[marcadorFila][i], inComponent: (i - inicio), animated: true)
        }
    calculoVolumen()
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 3
        }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch marcadorFila {
        case 0...5, 8:
            switch component {
            case 0:
                return centenas.count
            case 1:
                return decenas.count
            default:
                return unidades.count
            }
    
        default:
            switch component // 6, 7, 9, 10
            {
            case 0:
                return decenas.count
            case 1:
                return unidades.count
            default:
                return decimas.count
            }
        }
 
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
            
        var label = UILabel()
        if let v = view {
            label = v as! UILabel
        }
        label.font = UIFont (name: "Helvetica Neue", size: 21)
        //label.text =  decenas[row]
        label.textAlignment = .center
        label.textColor = UIColor.blue.withAlphaComponent(1)
        
        switch marcadorFila {

        case 0:  //Sodio
            switch component {
            case 0:
                label.text =  centenas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasActualesIones[0][1] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            case 1:
                label.text =  decenas[row]
            default:
                label.text =  unidades[row]
            }
            
        case 1:  //Cloruro sódico
            switch component {
            case 0:
                label.text =  centenas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasActualesIones[1][1] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            case 1:
                label.text =  decenas[row]
            default:
                label.text =  unidades[row]
            }
            
        case 2:  //Acetato sódico
            switch component {
            case 0:
                label.text =  centenas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasActualesIones[2][1] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            case 1:
                label.text =  decenas[row]
            default:
                label.text =  unidades[row]
            }
        
        case 3:  //Potasio
            switch component {
            case 0:
                label.text =  centenas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasActualesIones[3][1] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            case 1:
                label.text =  decenas[row]
            default:
                label.text =  unidades[row]
            }
            
        case 4:  //Cloruro potásico
            switch component {
            case 0:
                label.text =  centenas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasActualesIones[4][1] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            case 1:
                label.text =  decenas[row]
            default:
                label.text =  unidades[row]
            }
            
        case 5:  //Acetato potásico
            switch component {
            case 0:
                label.text =  centenas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasActualesIones[5][1] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            case 1:
                label.text =  decenas[row]
            default:
                label.text =  unidades[row]
            }
        
        case 6:  //Fósforo
            switch component {
            case 0:
                label.text =  decenas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasActualesIones[6][2] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            case 1:
                label.text =  unidades[row]
            default:
                label.text =  decimas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasActualesIones[6][4] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            }
            
            
        case 7:  //Fosfato monopotásico
            switch component {
            case 0:
                label.text =  decenas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasActualesIones[7][2] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            case 1:
                label.text =  unidades[row]
            default:
                label.text =  decimas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasActualesIones[7][4] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            }
            
        case 8:  //Fosfato dipotásico
            switch component {
            case 0:
                label.text =  decenas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasActualesIones[8][2] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            case 1:
                label.text =  unidades[row]
            default:
                label.text =  decimas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasActualesIones[8][4] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            }
            
        case 9:  //Cloro
            switch component {
            case 0:
                label.text =  centenas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasActualesIones[9][1] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            case 1:
                label.text =  decenas[row]
            default:
                label.text =  unidades[row]
            }
            
        case 10:  //Calcio
            switch component {
            case 0:
                label.text =  decenas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasActualesIones[10][2] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            case 1:
                label.text =  unidades[row]
            default:
                label.text =  decimas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasActualesIones[10][4] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            }
            
        case 11:  //Magnesio
            switch component {
            case 0:
                label.text =  decenas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasActualesIones[11][2] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            case 1:
                label.text =  unidades[row]
            default:
                label.text =  decimas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasActualesIones[11][4] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            }
        case 12...19:  //Volúmenes
            switch component {
            case 0:
                label.text =  centenas[row]
                label.textColor = UIColor.blue.withAlphaComponent(0.4)
                
            case 1:
                label.text =  decenas[row]
                label.textColor = UIColor.blue.withAlphaComponent(0.4)

            default:
                label.text =  unidades[row]
                label.textColor = UIColor.blue.withAlphaComponent(0.4)

            }

        default:
            break
        }
        return label
        }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        picker01.reloadAllComponents()
        listadoCamas[Int(cama)].valoresAntiguosIones = listadoCamas[Int(cama)].valoresActualesIones
        listadoCamas[Int(cama)].valoresAntiguosIones = listadoCamas[Int(cama)].valoresActualesIones
        
        
        posicionesGuardadasAntiguas = listadoCamas[Int(cama)].posicionesGuardadasActualesIones
        switch marcadorFila {
            case 0...5, 9:
                // Número tipo: 999
                switch component {
                    case 0:
                        let indice1 = picker01.selectedRow(inComponent: 0)
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[marcadorFila][1] = indice1
                    case 1:
                        let indice2 = picker01.selectedRow(inComponent: 1)
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[marcadorFila][2] = indice2
                    default:
                        let indice3 = picker01.selectedRow(inComponent: 2)
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[marcadorFila][3] = indice3
                    }
                let valorSumado = Double(centenas[listadoCamas[Int(cama)].posicionesGuardadasActualesIones[marcadorFila][1]])! + Double (decenas[listadoCamas[Int(cama)].posicionesGuardadasActualesIones[marcadorFila][2]])! + Double(unidades[listadoCamas[Int(cama)].posicionesGuardadasActualesIones[marcadorFila][3]])!
                listadoCamas[Int(cama)].valoresActualesIones[marcadorFila] = valorSumado
            
            
            default: // 6, 7, 8, 10, 11:
                // Número tipo: 99,9
                switch component {
                    case 0:
                        let indice2 = picker01.selectedRow(inComponent: 0)
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[marcadorFila][2] = indice2
                    case 1:
                        let indice3 = picker01.selectedRow(inComponent: 1)
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[marcadorFila][3] = indice3
                    default:
                        let indice4 = picker01.selectedRow(inComponent: 2)
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[marcadorFila][4] = indice4
                    }
                
                let valorSumado = Double(decenas[listadoCamas[Int(cama)].posicionesGuardadasActualesIones[marcadorFila][2]])! + Double (unidades[listadoCamas[Int(cama)].posicionesGuardadasActualesIones[marcadorFila][3]])! + Double(decimas[listadoCamas[Int(cama)].posicionesGuardadasActualesIones[marcadorFila][4]])!
                listadoCamas[Int(cama)].valoresActualesIones[marcadorFila] = valorSumado
        }
       

        //desdoblarvaloresActuales()
        calculoParametros()
        
    }
    func conversores(valor1: Double, valor2: Double) -> Double{
        switch (valor1, valor2) {
            case (0.0, 0.0):
                return 0.5
            case (0.0, 0.1...):
                return 0.0
            case( 0.1..., 0):
                return 1
            default:
                return (valor1 / (valor1 + valor2))
        }
        
    }
 
    func calculosIon(ion: Double, sal01: Double, sal02: Double, fosfMono: Double, fosfDi: Double, pos01: Int, pos02: Int, aviso: String) -> (Double, Double) {
        let ionCalculos = ion - (fosfMono + (2 * fosfDi))


            switch (sal01, sal02) {
                case (0.0, 0.0):
                    let resultado01 = ionCalculos * 0.5
                    let resultado02 = ionCalculos * 0.5
                    return (resultado01, resultado02)
                case (0.0, 0.1...):
                    let resultado01 = 0.0
                    let resultado02 = ionCalculos
                return (resultado01, resultado02)

                case( 0.1..., 0):
                    let resultado01 = ionCalculos
                    let resultado02 = 0.0
                    return (resultado01, resultado02)
                default:
                    let conversor01 = sal01 / (sal01 + sal02)
                    let conversor02 = 1 - conversor01
                    let resultado01 = ionCalculos * conversor01
                    let resultado02 = ionCalculos * conversor02
                    return (resultado01, resultado02)
                }
        
            }
    
    func calculosSal(ion: Double, sal01: Double, sal02: Double, fosfMono: Double, fosfDi: Double, pos01: Int, pos02: Int, aviso: String) -> (Double, Double)  {
       
        let ionCalculos = ion - (fosfMono + (2 * fosfDi))
        let resultado01 = sal01
        let resultado02 = ionCalculos - sal01
        //reasignarValoresActuales()
        return (resultado01, resultado02)
        /*
        
        if (sal01 > ionCalculos || sal02 > ionCalculos || sal01 < 0 || sal02 < 0) {
            alerta(aviso: aviso)
            //listadoCamas[Int(cama)].valoresActualesIones[pos01] = listadoCamas[Int(cama)].valoresAntiguosIones[pos01]
            //listadoCamas[Int(cama)].valoresActualesIones[pos02] = listadoCamas[Int(cama)].valoresAntiguosIones[pos02]
            listadoCamas[Int(cama)].valoresActualesIones = listadoCamas[Int(cama)].valoresAntiguosIones
            posicionesGuardadasActualesIones = posicionesGuardadasAntiguas
            
           // print("Valores antiguos: \(listadoCamas[Int(cama)].valoresActualesIones[pos01]). \(listadoCamas[Int(cama)].valoresActualesIones[pos02])")

            let resultado01 = listadoCamas[Int(cama)].valoresAntiguosIones[pos01]
            let resultado02  = listadoCamas[Int(cama)].valoresAntiguosIones[pos02]
            situarPicker()
            return (resultado01, resultado02)
        }
        else {
            let resultado01 = sal01
            let resultado02 = ionCalculos - sal01
            reasignarValoresActuales()
            return (resultado01, resultado02)
        }
 */
        }
    func alerta(aviso: String){
        let alert = UIAlertController(title: aviso, message: nil, preferredStyle: .alert)
       alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        
       // alert.addTextField(configurationHandler: { textField in textField.placeholder = "Input your name here..." })
        
        //alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action inif let name = alert.textFields?.first?.text { print("Your name: \(name)")} }))
        
        self.present(alert, animated: true)
       // reasignarValoresActuales()
        
        
        
    }
    
    func calculoParametros(){
        let conversorSodio = conversores(valor1: listadoCamas[Int(cama)].valoresAntiguosIones[1], valor2: listadoCamas[Int(cama)].valoresAntiguosIones[2])
        let conversorPotasio = conversores(valor1: listadoCamas[Int(cama)].valoresAntiguosIones[4], valor2: listadoCamas[Int(cama)].valoresAntiguosIones[5])
        let conversorSodioPotasio = conversores(valor1: listadoCamas[Int(cama)].valoresAntiguosIones[1], valor2: listadoCamas[Int(cama)].valoresAntiguosIones[4])
        let conversorFosforo = conversores(valor1: listadoCamas[Int(cama)].valoresAntiguosIones[7], valor2: listadoCamas[Int(cama)].valoresAntiguosIones[8])
        
        
        switch marcadorFila {
            case 0:
                let aviso = "Nada"
                listadoCamas[Int(cama)].valoresActualesIones[1] = listadoCamas[Int(cama)].valoresActualesIones[0] * conversorSodio
                listadoCamas[Int(cama)].valoresActualesIones[2] = listadoCamas[Int(cama)].valoresActualesIones[0] * (1 - conversorSodio)
                listadoCamas[Int(cama)].valoresActualesIones[9] = listadoCamas[Int(cama)].valoresActualesIones[1] + listadoCamas[Int(cama)].valoresActualesIones[4]

            case 1:
                let aviso = "La cantidad de Cloruro Sódico no es compatible con el Sodio elegido. \nModifica antes la cantidad de Sodio total si quieres mantenerla."
                listadoCamas[Int(cama)].valoresActualesIones[0] = listadoCamas[Int(cama)].valoresActualesIones[1] + listadoCamas[Int(cama)].valoresActualesIones[2]
                listadoCamas[Int(cama)].valoresActualesIones[9] = listadoCamas[Int(cama)].valoresActualesIones[1] + listadoCamas[Int(cama)].valoresActualesIones[4]

            
            case 2:
                let aviso = "La cantidad de Acetato Sódico no es compatible con el Sodio elegido. \nModifica antes la cantidad de Sodio total si quieres mantenerla."
                listadoCamas[Int(cama)].valoresActualesIones[0] = listadoCamas[Int(cama)].valoresActualesIones[1] + listadoCamas[Int(cama)].valoresActualesIones[2]
                listadoCamas[Int(cama)].valoresActualesIones[9] = listadoCamas[Int(cama)].valoresActualesIones[1] + listadoCamas[Int(cama)].valoresActualesIones[4]


            case 3:
                let aviso = "Nada"
                let potasioCalculos = listadoCamas[Int(cama)].valoresActualesIones[3] - ( listadoCamas[Int(cama)].valoresActualesIones[7] + listadoCamas[Int(cama)].valoresActualesIones[8] * 2)
                listadoCamas[Int(cama)].valoresActualesIones[4] = potasioCalculos * conversorPotasio
                listadoCamas[Int(cama)].valoresActualesIones[5] = potasioCalculos * (1 - conversorPotasio)
                listadoCamas[Int(cama)].valoresActualesIones[9] = listadoCamas[Int(cama)].valoresActualesIones[1] + listadoCamas[Int(cama)].valoresActualesIones[4]
            
            case 4:
                let aviso = "La cantidad de Cloruro Potásico no es compatible con el Potasio elegido.\nRecuerda que en el Potasio total se añade el administrado como fosfato. \nModifica antes la cantidad de Potasio total si quieres mantenerla."
                listadoCamas[Int(cama)].valoresActualesIones[3] = listadoCamas[Int(cama)].valoresActualesIones[4] + listadoCamas[Int(cama)].valoresActualesIones[5] + listadoCamas[Int(cama)].valoresActualesIones[7] + listadoCamas[Int(cama)].valoresActualesIones[8] * 2
                listadoCamas[Int(cama)].valoresActualesIones[9] = listadoCamas[Int(cama)].valoresActualesIones[1] + listadoCamas[Int(cama)].valoresActualesIones[4]

            
            case 5:
                let aviso = "La cantidad de Acetato Potásico no es compatible con el Potasio elegido.\nRecuerda que en el Potasio total se añade el administrado como fosfato.\nModifica antes la cantidad de Potasio total si quieres mantenerla."
                 listadoCamas[Int(cama)].valoresActualesIones[3] = listadoCamas[Int(cama)].valoresActualesIones[4] + listadoCamas[Int(cama)].valoresActualesIones[5] + listadoCamas[Int(cama)].valoresActualesIones[7] + listadoCamas[Int(cama)].valoresActualesIones[8] * 2
                listadoCamas[Int(cama)].valoresActualesIones[9] = listadoCamas[Int(cama)].valoresActualesIones[1] + listadoCamas[Int(cama)].valoresActualesIones[4]

           
            case 6:
                let aviso = "La cantidad de Fósforo no es compatible con la cantidad de Potasio elegida.\nModifica la cantidad de Fósforo, el tipo de fosfato o la cantidad de Potasio elegidos"
                listadoCamas[Int(cama)].valoresActualesIones[7] = listadoCamas[Int(cama)].valoresActualesIones[6] * conversorFosforo
                listadoCamas[Int(cama)].valoresActualesIones[8] = listadoCamas[Int(cama)].valoresActualesIones[6] * (1 - conversorFosforo)
                
                let potasioCalculos = listadoCamas[Int(cama)].valoresActualesIones[3] - (listadoCamas[Int(cama)].valoresActualesIones[7] + listadoCamas[Int(cama)].valoresActualesIones[8] * 2)
                listadoCamas[Int(cama)].valoresActualesIones[4] = potasioCalculos * conversorPotasio
                listadoCamas[Int(cama)].valoresActualesIones[5] = potasioCalculos * (1 - conversorPotasio)
                listadoCamas[Int(cama)].valoresActualesIones[9] = listadoCamas[Int(cama)].valoresActualesIones[1] + listadoCamas[Int(cama)].valoresActualesIones[4]

            case 7:
                let aviso = "La cantidad de Fosfato Monopotásico no es compatible con el Fósforo elegido. \nModifica antes la cantidad de Fósforo total si quieres mantenerla."
                listadoCamas[Int(cama)].valoresActualesIones[6] = listadoCamas[Int(cama)].valoresActualesIones[7] + listadoCamas[Int(cama)].valoresActualesIones[8]
               
                let potasioCalculos = listadoCamas[Int(cama)].valoresActualesIones[3] - (listadoCamas[Int(cama)].valoresActualesIones[7] + listadoCamas[Int(cama)].valoresActualesIones[8] * 2)
                listadoCamas[Int(cama)].valoresActualesIones[4] = potasioCalculos * conversorSodioPotasio
                listadoCamas[Int(cama)].valoresActualesIones[5] = potasioCalculos * (1 - conversorSodioPotasio)
                listadoCamas[Int(cama)].valoresActualesIones[9] = listadoCamas[Int(cama)].valoresActualesIones[1] + listadoCamas[Int(cama)].valoresActualesIones[4]
            
            
            case 8:
                let aviso = "La cantidad de Fosfato Dipotásico no es compatible con el Fósforo elegido. \nModifica antes la cantidad de Fósforo total si quieres mantenerla."
                listadoCamas[Int(cama)].valoresActualesIones[6] = listadoCamas[Int(cama)].valoresActualesIones[7] + listadoCamas[Int(cama)].valoresActualesIones[8]
                
                let potasioCalculos = listadoCamas[Int(cama)].valoresActualesIones[3] - (listadoCamas[Int(cama)].valoresActualesIones[7] + listadoCamas[Int(cama)].valoresActualesIones[8] * 2)
                listadoCamas[Int(cama)].valoresActualesIones[4] = potasioCalculos * conversorSodioPotasio
                listadoCamas[Int(cama)].valoresActualesIones[5] = potasioCalculos * (1 - conversorSodioPotasio)
                listadoCamas[Int(cama)].valoresActualesIones[9] = listadoCamas[Int(cama)].valoresActualesIones[1] + listadoCamas[Int(cama)].valoresActualesIones[4]
        case 9:
            
            let aviso = "La cantidad de Fósforo no es compatible con la cantidad de Potasio elegida.\nModifica la cantidad de Fósforo, el tipo de fosfato o la cantidad de Potasio elegidos"
            listadoCamas[Int(cama)].valoresActualesIones[1] = listadoCamas[Int(cama)].valoresActualesIones[9] * conversorSodioPotasio
            listadoCamas[Int(cama)].valoresActualesIones[2] = listadoCamas[Int(cama)].valoresActualesIones[0] - listadoCamas[Int(cama)].valoresActualesIones[1]
            listadoCamas[Int(cama)].valoresActualesIones[4] = listadoCamas[Int(cama)].valoresActualesIones[9] - listadoCamas[Int(cama)].valoresActualesIones[1]
            listadoCamas[Int(cama)].valoresActualesIones[5] = listadoCamas[Int(cama)].valoresActualesIones[3] - listadoCamas[Int(cama)].valoresActualesIones[4] - listadoCamas[Int(cama)].valoresActualesIones[7] - listadoCamas[Int(cama)].valoresActualesIones[8]


        default:
                break
            }
        chequeoValoresAdecuados()
    }
    
    func chequeoValoresAdecuados(){
        if listadoCamas[Int(cama)].valoresActualesIones[1] < 0 || listadoCamas[Int(cama)].valoresActualesIones[2] < 0 {
            alerta(aviso: "La cantidad elegida no es compatible con el Sodio total.\nModifica antes la cantidad de Sodio total si quieres mantenerla.")
            listadoCamas[Int(cama)].valoresActualesIones = listadoCamas[Int(cama)].valoresAntiguosIones
            listadoCamas[Int(cama)].posicionesGuardadasActualesIones = posicionesGuardadasAntiguas
            situarPicker()
        }
            /*
        else if listadoCamas[Int(cama)].valoresActualesIones[4] < 0 || listadoCamas[Int(cama)].valoresActualesIones[5] < 0 {
            if (listadoCamas[Int(cama)].valoresActualesIones[7] + listadoCamas[Int(cama)].valoresActualesIones[8] * 2) > listadoCamas[Int(cama)].valoresActualesIones[3] {
                alerta(aviso: "La cantidad elegida no es compatible con el Potasio total.\nRecuerda que en el Potasio total se añade el administrado como fosfato.\nModifica antes la cantidad de Potasio total o el tipo de fosfato,si es posible, si quieres mantenerla.")
            }
                else {
                    alerta(aviso: "La cantidad elegida no es compatible con el Potasio total.\nRecuerda que en el Potasio total se añade el administrado como fosfato. \nModifica antes la cantidad de Potasio total si quieres mantenerla.")
                }
            listadoCamas[Int(cama)].valoresActualesIones = listadoCamas[Int(cama)].valoresAntiguosIones
            posicionesGuardadasActualesIones = posicionesGuardadasAntiguas
            situarPicker()
        }
 */
        else if listadoCamas[Int(cama)].valoresActualesIones[7] > listadoCamas[Int(cama)].valoresActualesIones[6] || listadoCamas[Int(cama)].valoresActualesIones[8] > listadoCamas[Int(cama)].valoresActualesIones[6] {
            alerta(aviso: "La cantidad elegida no es compatible con el Fósforo elegido.\nModifica antes la cantidad de Fósforo total si quieres mantenerla.")
            listadoCamas[Int(cama)].valoresActualesIones = listadoCamas[Int(cama)].valoresAntiguosIones
            listadoCamas[Int(cama)].posicionesGuardadasActualesIones = listadoCamas[Int(cama)].posicionesGuardadasAntiguasIones
            situarPicker()
        }
        
        else if (listadoCamas[Int(cama)].valoresActualesIones[7] + listadoCamas[Int(cama)].valoresActualesIones[8] * 2) > listadoCamas[Int(cama)].valoresActualesIones[3] {
            alerta(aviso: "La cantidad elegida no es compatible con el Potasio total.\nRecuerda que en el Potasio total se añade el administrado como fosfato.\nModifica antes la cantidad de Potasio total o el tipo de fosfato,si es posible, si quieres mantenerla.")
            listadoCamas[Int(cama)].valoresActualesIones = listadoCamas[Int(cama)].valoresAntiguosIones
            listadoCamas[Int(cama)].posicionesGuardadasActualesIones = listadoCamas[Int(cama)].posicionesGuardadasAntiguasIones
            situarPicker()
        }
   
        asignarIndices()
    }
    /*
    Asignación de valores
        listadoCamas[Int(cama)].valoresActualesIones[0] = sodio
        listadoCamas[Int(cama)].valoresActualesIones[1] = clorurosodico
        listadoCamas[Int(cama)].valoresActualesIones[2] = acetatosodico
        listadoCamas[Int(cama)].valoresActualesIones[3] = potasio
        listadoCamas[Int(cama)].valoresActualesIones[4] = cloruropotasico
        listadoCamas[Int(cama)].valoresActualesIones[5] = acetatopotasico
        listadoCamas[Int(cama)].valoresActualesIones[6] = fosforo
        listadoCamas[Int(cama)].valoresActualesIones[7] = fostatomonopotasico
        listadoCamas[Int(cama)].valoresActualesIones[8] = fostatodipotasico
        listadoCamas[Int(cama)].valoresActualesIones[9] = cloro
        listadoCamas[Int(cama)].valoresActualesIones[10] = gluconatocalcico
        listadoCamas[Int(cama)].valoresActualesIones[11] = sultatomagnesio
        listadoCamas[Int(cama)].valoresActualesIones[12] = volHdeC
        listadoCamas[Int(cama)].valoresActualesIones[13] = volLipidos
        listadoCamas[Int(cama)].valoresActualesIones[14] = volProteinas
        listadoCamas[Int(cama)].valoresActualesIones[15] = volIones
        listadoCamas[Int(cama)].valoresActualesIones[16] = volGlutamina
        listadoCamas[Int(cama)].valoresActualesIones[17] = volOligoelementos
        listadoCamas[Int(cama)].valoresActualesIones[18] = volPolivitaminico
        listadoCamas[Int(cama)].valoresActualesIones[19] = volMinimmo

    */
    
    func asignarIndices(){
        
        for i in 0...listadoCamas[Int(cama)].valoresActualesIones.count - 1 {
            if i != marcadorFila {
                
                let valor: Double = listadoCamas[Int(cama)].valoresActualesIones[i]
                if valor != 0 && valor.isFinite{
                    
                    switch i {
                    case 0...5, 9: // Numero sin decimales
                        let valorReconvertido = Int(round(valor))
                        
                        let indice0 = Int((valorReconvertido / 1000))
                        let resto0 = indice0 * 1000
                        
                        let indice1 = Int(((valorReconvertido - resto0)) / 100)
                        let resto1 = resto0 + indice1 * 100
                        
                        let indice2 = Int(((valorReconvertido - resto1)) / 10)
                        let resto2 = resto1 + indice2 * 10
                        
                        let indice3 = Int((valorReconvertido - resto2))
                        
                        let indice4 = 0
                        let indice5 = 0
                        
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][0] = indice0
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][1] = indice1
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][2] = indice2
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][3] = indice3
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][4] = indice4
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][5] = indice5
                        
                        
                        
                    case 6, 7, 8, 10, 11: //Número con un decimal
                        let valorReconvertido = Int(round(valor * 10))
                        
                        let indice0 = Int((valorReconvertido / 10000))
                        let resto0 = indice0 * 10000
                        
                        let indice1 = Int(((valorReconvertido - resto0)) / 1000)
                        let resto1 = resto0 + indice1 * 1000
                        
                        let indice2 = Int(((valorReconvertido - resto1)) / 100)
                        let resto2 = resto1 + indice2 * 100
                        
                        let indice3 = Int((valorReconvertido - resto2) / 10)
                        let resto3 = resto2 + indice3 * 10
                        
                        let indice4 = Int((valorReconvertido - resto3))
                        
                        let indice5 = 0
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][0] = indice0
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][1] = indice1
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][2] = indice2
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][3] = indice3
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][4] = indice4
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][5] = indice5
                        
                        
                    default:
                       break
                    }
                }
            }
        }

        calculoVolumen()
    }
    
    
    func calculoVolumen(){
        /*
        let camasOut = UserDefaults.standard.data(forKey: "ListadoCamas")
        listadoCamas = try! JSONDecoder().decode([Nutricion].self, from: camasOut!)
        */
 
        
        
        
        //let valoresActualesNutrientes = UserDefaults.standard.array(forKey: "NutricionValoresActualesNutrientes") as! [Double]
        listadoCamas[Int(cama)].valoresActualesIones[12] = listadoCamas[Int(cama)].valoresActualesNutricion[5] * 2 //H. de C.
        listadoCamas[Int(cama)].valoresActualesIones[13] = listadoCamas[Int(cama)].valoresActualesNutricion[6] * 5 //Lípidos.
        listadoCamas[Int(cama)].valoresActualesIones[14] = listadoCamas[Int(cama)].valoresActualesNutricion[7] * 10 //Proteinas.
        
        
        listadoCamas[Int(cama)].valoresActualesIones[15] = listadoCamas[Int(cama)].valoresActualesIones[1] + listadoCamas[Int(cama)].valoresActualesIones[1] + listadoCamas[Int(cama)].valoresActualesIones[2] + listadoCamas[Int(cama)].valoresActualesIones[4] + listadoCamas[Int(cama)].valoresActualesIones[5] + listadoCamas[Int(cama)].valoresActualesIones[7] + listadoCamas[Int(cama)].valoresActualesIones[10] + listadoCamas[Int(cama)].valoresActualesIones[11]//Iones.
        listadoCamas[Int(cama)].valoresActualesIones[19] = listadoCamas[Int(cama)].valoresActualesIones[12] + listadoCamas[Int(cama)].valoresActualesIones[13] + listadoCamas[Int(cama)].valoresActualesIones[14] + listadoCamas[Int(cama)].valoresActualesIones[15] + listadoCamas[Int(cama)].valoresActualesIones[16] + listadoCamas[Int(cama)].valoresActualesIones[17] + listadoCamas[Int(cama)].valoresActualesIones[18]//Volumen mínimo.

        rellenarCampos()
    }
   
    
    func rellenarCampos(){
        valores[0][0] = String(format: "%.0f", listadoCamas[Int(cama)].valoresActualesIones[0])
        valores[0][1] = String(format: "%.0f", listadoCamas[Int(cama)].valoresActualesIones[1])
        valores[0][2] = String(format: "%.0f", listadoCamas[Int(cama)].valoresActualesIones[2])
        valores[0][3] = String(format: "%.0f", listadoCamas[Int(cama)].valoresActualesIones[3])
        valores[0][4] = String(format: "%.0f", listadoCamas[Int(cama)].valoresActualesIones[4])
        valores[0][5] = String(format: "%.0f", listadoCamas[Int(cama)].valoresActualesIones[5])
        valores[0][6] = String(format: "%.1f", listadoCamas[Int(cama)].valoresActualesIones[6])
        valores[0][7] = String(format: "%.1f", listadoCamas[Int(cama)].valoresActualesIones[7])
        valores[0][8] = String(format: "%.1f", listadoCamas[Int(cama)].valoresActualesIones[8])
        valores[0][9] = String(format: "%.0f", listadoCamas[Int(cama)].valoresActualesIones[9])
        valores[0][10] = String(format: "%.1f", listadoCamas[Int(cama)].valoresActualesIones[10])
        valores[0][11] = String(format: "%.1f", listadoCamas[Int(cama)].valoresActualesIones[11])
        valores[1][3] = String(format: "%.0f", listadoCamas[Int(cama)].valoresActualesIones[12])
        valores[1][4] = String(format: "%.0f", listadoCamas[Int(cama)].valoresActualesIones[13])
        valores[1][5] = String(format: "%.0f", listadoCamas[Int(cama)].valoresActualesIones[14])
        valores[1][6] = String(format: "%.0f", listadoCamas[Int(cama)].valoresActualesIones[15])
        valores[1][7] = String(format: "%.0f", listadoCamas[Int(cama)].valoresActualesIones[19])
        if listadoCamas[Int(cama)].valoresActualesIones[15] == 5{
  
        }
        let camasIn = try! JSONEncoder().encode(listadoCamas)
        UserDefaults.standard.set(camasIn, forKey: "ListadoCamas")
        tabla.reloadData()
    }
    
    

}

    

