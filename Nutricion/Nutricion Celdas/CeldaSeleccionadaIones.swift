//
//  CeldaSeleccionadaIones.swift
//  Nutricion
//
//  Created by Julio Barado Hualde on 05/04/2019.
//  Copyright © 2019 Julio Barado Hualde. All rights reserved.
//

import UIKit

class CeldaSeleccionadaIones: UITableViewCell {

    @IBOutlet weak var etiquetas: UILabel!
    @IBOutlet weak var valores: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        etiquetas.textColor = .red
        valores.textColor = .red
        
        etiquetas.textColor = .red
        etiquetas.font = UIFont.boldSystemFont(ofSize: 19.0)
        valores.textColor = .red
        valores.font = UIFont.boldSystemFont(ofSize: 19.0)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        etiquetas.textColor = .red
        valores.textColor = .red
        
        etiquetas.textColor = .red
        etiquetas.font = UIFont.boldSystemFont(ofSize: 19.0)
        valores.textColor = .red
        valores.font = UIFont.boldSystemFont(ofSize: 19.0)
        // Configure the view for the selected state
    }

}
