//
//  CeldaTitulo.swift
//  Nutricion
//
//  Created by Julio Barado Hualde on 05/04/2019.
//  Copyright © 2019 Julio Barado Hualde. All rights reserved.
//

import UIKit

class CeldaTitulo: UITableViewCell {

    @IBOutlet weak var etiquetas: UILabel!
    
    @IBOutlet weak var valores: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        etiquetas.textColor = .black
        etiquetas.font = UIFont.boldSystemFont(ofSize: 19.0)
        valores.textColor = .black
        valores.font = UIFont.boldSystemFont(ofSize: 19.0)
        
 

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        etiquetas.textColor = .black
        etiquetas.font = UIFont.boldSystemFont(ofSize: 19.0)
        valores.textColor = .black
        valores.font = UIFont.boldSystemFont(ofSize: 19.0)
        
    }
}
