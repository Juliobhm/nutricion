//
//  NutricionConstantes.swift
//  Nutricion
//
//  Created by Julio Barado Hualde on 29/04/2019.
//  Copyright © 2019 Julio Barado Hualde. All rights reserved.
//

import Foundation
    var cama: Double = 1.0
    class Nutricion: Codable {
        var id: Double
        var datosGrabados: Int
        var valoresActualesNutricion: [Double]
        var valoresActualesIones: [Double]
        var valoresAntiguosIones: [Double]
        var posicionesGuardadasNutricion: [[Int]]
        var posicionesGuardadasActualesIones: [[Int]]
        var posicionesGuardadasAntiguasIones: [[Int]]
        var checked: [Bool]
        
        init(id : Double, datosGrabados: Int, valoresActualesNutricion: [Double], valoresActualesIones: [Double], valoresAntiguosIones: [Double], posicionesGuardadasNutricion: [[Int]], posicionesGuardadasActualesIones: [[Int]], posicionesGuardadasAntiguasIones: [[Int]], checked: [Bool]) {
            self.id = id
            self.datosGrabados = datosGrabados
            self.valoresActualesNutricion = valoresActualesNutricion
            self.valoresActualesIones = valoresActualesIones
            self.valoresAntiguosIones = valoresAntiguosIones
            
            self.posicionesGuardadasNutricion = posicionesGuardadasNutricion
            self.posicionesGuardadasActualesIones = posicionesGuardadasActualesIones
            self.posicionesGuardadasAntiguasIones = posicionesGuardadasAntiguasIones
            self.checked = checked
    }
    }
    var listadoCamas: [Nutricion] = []

    var millares = ["0000", "1000","2000","3000","4000","5000","6000","7000","8000","9000"]
    var centenas = ["000", "100","200","300","400","500","600","700","800","900"]
    var decenas = ["00", "10","20","30","40","50","60","70","80","90"]
    var unidades = ["0","1","2","3","4","5","6","7","8","9"]
    var decimas = ["0.0","0.1","0.2","0.3","0.4","0.5","0.6","0.7","0.8","0.9"]
    var centesimas = ["0.00","0.01","0.02","0.03","0.04","0.05","0.06","0.07","0.08","0.09"]
var menuShowing = false


