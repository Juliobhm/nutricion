//
//  NutricionPageViewController.swift
//  Nutricion
//
//  Created by Julio Barado Hualde on 04/04/2019.
//  Copyright © 2019 Julio Barado Hualde. All rights reserved.
//

import UIKit

class NutricionPageViewController: UIPageViewController, UIPageViewControllerDataSource {

    lazy var viewControllerList: [UIViewController] = {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc1 = sb.instantiateViewController(withIdentifier: "NutrientesVC")
        let vc2 = sb.instantiateViewController(withIdentifier: "IonesVC")
        
        return [vc1, vc2]
        
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        if let firstViewController = viewControllerList.first {
            self.setViewControllers([firstViewController], direction: .forward, animated: true, completion: nil)    }
        
        for reconicer in self.gestureRecognizers {
            if reconicer is UISwipeGestureRecognizer {
                reconicer.isEnabled = false}
            }
            
        
        for view in self.view.subviews {
            if view is UIScrollView {
                let scrollView = view as! UIScrollView
                scrollView.isScrollEnabled = false;
            } else {
                print("no scroll view??")
            }
        }
        
    }
            

    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = viewControllerList.firstIndex(of: viewController) else {return nil}
        let previousIndex = vcIndex - 1
        guard previousIndex >= 0 else {return nil}
        guard previousIndex <= viewControllerList.count else {return nil}
        return viewControllerList[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = viewControllerList.firstIndex(of: viewController) else {return nil}
        let nextIndex = vcIndex + 1
        guard nextIndex >= 0 else {return nil}
        guard nextIndex < viewControllerList.count else {return nil}
        return viewControllerList[nextIndex]
    }

    

}
