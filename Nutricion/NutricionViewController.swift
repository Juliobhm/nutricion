//
//  NutricionViewController.swift
//  Nutricion01
//
//  Created by Julio Barado Hualde on 06/02/2019.
//  Copyright © 2019 Julio Barado Hualde. All rights reserved.
//

import UIKit
class NutricionViewController: UIViewController,  UITableViewDelegate, UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet var PrincipalView: UIView!
    @IBOutlet weak var tabla: UITableView!
    @IBOutlet weak var marcoSliderView: UIView!
    @IBOutlet weak var barraCloseView: UIView!
    var secciones = ["Cama seleccionada", "Datos fisiológicos", "Valores  necesarios", "Calorias", "Proteinas"]
    var etiquetas = [["Cama seleccionada"], ["Edad (años)", "Peso actual (kg)", "Límite obesidad (kg)", "Peso elegido: ", "Talla (cm)", "Sexo", "Gasto energético basal"],
                     ["Hidratos de carbono (g)", "Lípidos (g)", "Nitrógeno (g)"],
                     ["Calorías Totales", "Calorías / kg", "Porcentaje del GEB", "Calorías No Proteicas", "Cal. Glucosa / Lípidos (%)", "Cal Glu/ Lip/Prot (%)","Gramos de glucosa por kg", "Gramos de lípidos por kg"],
                     ["Calorías NP / gramo N2", "Gramos de proteína por kg"]]
    var valores = [["Cama seleccionada"], ["Edad (años)", "Peso actual (kg)", "Límite obesidad (kg)", "Peso elegido: ", "Talla (cm)", "Sexo", "Gasto energético basal"],
                   ["Hidratos de carbono (g)", "Lípidos (g)", "Nitrógeno (g)"],
                   ["Calorías Totales", "Calorías / kg", "Porcentaje del GEB", "Calorías No Proteicas", "Cal. Glucosa / Lípidos", "Cal. Glu/Lip/Prot (%)", "Gramos de glucosa por kg", "Gramos de lípidos por kg"],
                   ["Calorías NP / gramo N2", "Gramos de proteína por kg"]]
    
    /*
    var millares: [Double] = []
    var centenas: [Double] = []
    var decenas: [Double] = []
    var unidades: [Double] = []
    var decimas: [Double] = []
    var centesimas: [Double] = []
    var sexos = [1.0, 2.0]
    */
    var valorPicker = 0.0
    var marcadorFila = 0
    var tipoPicker: Int = 1
    var posicionesGuardadas: [[Int]] = []
    var valoresActuales: [Double] = []


   
 

    let sexos = ["Hombre", "Mujer"]
    let pesos = ["Peso actual", "Peso ideal", "Peso ajustado"]
    var numeroCama: [String] = []

    
    
    var picker01 = UIPickerView()
    var numCama: Double = 1.0
    var pesoActual: Double = 70
    var pesoIdeal: Double = 70
    var limiteObesidad: Double = 70
    var pesoAjustado: Double = 70
    var pesoElegido: Double = 0

    var pesoCalculos: Double = 70
    var talla: Double = 170
    var edad: Double = 50
    var sexo: Double = 0
    var harrisBenedict: Double = 1
    var nitrogeno: Double = 10
    var caloriasTotales: Double = 0
    var caloriasNoProteicas: Double = 0
    var calKg: Double = 0
    var caloriasBasal: Double = 0
    var calHarris: Double = 1
    var HdeCLipidos: Double = 50
    var HdeC: Double = 0
    var lipidos: Double = 0
    var calNPgN2: Double = 0
    var gramosProtKg: Double = 0
    var gramosGluKg: Double = 0
    var gramosLipidosKg: Double = 0
    var calGlucosa: Double = 0
    var calLipidos: Double = 0
    var calProteinas: Double = 0
    var indiceActual: IndexPath = []

    var valoresActuales01: [[[Double]]] = []
    
    @IBOutlet weak var alturaConstraint: NSLayoutConstraint!
  
    @IBOutlet weak var pgControlVeriticalConstraint: NSLayoutConstraint!
    @IBOutlet weak var pgControlCentralConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Generación del listado de camas
        for i in 0...24{
            let camaAsignada = String(i)
            numeroCama.append(camaAsignada)
        }
        
        picker01.delegate = self
        picker01.dataSource = self
        
        //Borrar Userdefaults
        //let bundleIdentifier = Bundle.main.bundleIdentifier!
        //UserDefaults.standard.removePersistentDomain(forName: bundleIdentifier)
        
        
        
        if let key = UserDefaults.standard.object(forKey: "ListadoCamas"){
            // existe archivo deafault
            let camasOut = UserDefaults.standard.data(forKey: "ListadoCamas")
            let lista = try! JSONDecoder().decode([Nutricion].self, from: camasOut!)
            listadoCamas = lista
            cama = 1
            desdoblarvaloresActuales(camaElegida: cama)
        }
        else {
            // no existe archivo default, se genera para todas las camas con valore standard
            creacionValoresBasales(inicio: 0, fin: 24)

        }

        // Selección de la fila de cama al inicio
       cama = 1
        let indice = IndexPath(row: 0, section: 0)
        tabla.selectRow(at: indice, animated: true, scrollPosition: .none)
        marcadorFila = 20 // Se recupera el marcadorFila que se quiere
        tipoPicker = 3 // Picker de cama
        mostrarPicker()
        rellenarCampos()
        
        let indexPath = IndexPath(row: 0, section: 1)
        tabla.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        self.tableView(self.tabla, didSelectRowAt: indexPath) // se invoca a la accion didseletrow de la tabla
        
        let swipeInferior = UISwipeGestureRecognizer(target: self, action: #selector(ocultarVentana(gesto:)))
        swipeInferior.direction = UISwipeGestureRecognizer.Direction.down
        barraCloseView.addGestureRecognizer(swipeInferior)
        
        let swipeSuperior = UISwipeGestureRecognizer(target: self, action: #selector(ocultarVentana(gesto:)))
        swipeSuperior.direction = UISwipeGestureRecognizer.Direction.up
        barraCloseView.addGestureRecognizer(swipeSuperior)
        
    }
    @objc func ocultarVentana(gesto: UISwipeGestureRecognizer){
        if let swipeGesto = gesto as? UISwipeGestureRecognizer{
            print("Gesto")
            switch swipeGesto.direction {
                case UISwipeGestureRecognizer.Direction.down:
                    alturaConstraint.constant = 0
                    pgControlCentralConstraint.constant = 145
                    pgControlVeriticalConstraint.constant = -10
                case UISwipeGestureRecognizer.Direction.up:
                    alturaConstraint.constant = 124
                    pgControlCentralConstraint.constant = 0
                    pgControlVeriticalConstraint.constant = -134
                default:
                    break
                }
            menuShowing = !menuShowing
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn,animations: {self.view.layoutIfNeeded()})
            }
    }
    func creacionValoresBasales(inicio: Int, fin: Int) {
        for i in inicio...fin{
            let nuevaCama = Nutricion(id: 0, datosGrabados: 0, valoresActualesNutricion: [0], valoresActualesIones: [0], valoresAntiguosIones: [0], posicionesGuardadasNutricion: [[0]], posicionesGuardadasActualesIones: [[0]], posicionesGuardadasAntiguasIones: [[0]], checked: [false]  )
            //listadoCamas.append(nuevaCama)
            nuevaCama.valoresActualesNutricion = Array(repeating: 0, count: 30)
            nuevaCama.valoresActualesIones = Array(repeating: 0, count: 30)
            nuevaCama.valoresAntiguosIones = Array(repeating: 0, count: 30)
            nuevaCama.posicionesGuardadasNutricion = Array(repeating: Array(repeating: 0, count: 6), count: 30)
            
            // El número de cama, en la posición 20, es inamovible
            nuevaCama.valoresActualesNutricion[20] = Double(i)
            // La posición de la cama es inamovible
            nuevaCama.posicionesGuardadasNutricion[20][3] = i


            nuevaCama.posicionesGuardadasActualesIones = Array(repeating: Array(repeating: 0, count: 6), count: 26)
            nuevaCama.posicionesGuardadasAntiguasIones = Array(repeating: Array(repeating: 0, count: 6), count: 26)
            // Los tres valores para los datos con checmaker en la tabla
            nuevaCama.checked = Array(repeating: true, count: 3)
            nuevaCama.checked[0] = true //Glutamina sin checkar
            nuevaCama.valoresActualesIones[17] = 10
            nuevaCama.valoresActualesIones[18] = 15

            
            listadoCamas.insert(nuevaCama, at: i)
            //listadoCamas.append(nuevaCama)
            cama = Double(i)
            edad = 50
            pesoActual = 70
            limiteObesidad = 79.9
            pesoElegido = 0
            pesoCalculos = 70
            talla = 170
            sexo = 0
            caloriasBasal = 1541
            HdeC = 100
            lipidos = 50
            nitrogeno = 10
            caloriasTotales = 1100
            calKg = 15.7
            calHarris = 0.71
            caloriasNoProteicas = 850
            HdeCLipidos = 47
            calProteinas = 23
            calGlucosa = 36
            calLipidos = 41
            gramosGluKg = 1.43
            gramosLipidosKg = 0.71
            calNPgN2 = 85
            gramosProtKg = 0.89
            
            //valores de iones
            listadoCamas[i].valoresActualesIones[0] = 100
            listadoCamas[i].valoresActualesIones[1] = 50
            listadoCamas[i].valoresActualesIones[2] = 50
            listadoCamas[i].valoresActualesIones[3] = 55
            listadoCamas[i].valoresActualesIones[4] = 20
            listadoCamas[i].valoresActualesIones[5] = 20
            listadoCamas[i].valoresActualesIones[6] = 15
            listadoCamas[i].valoresActualesIones[7] = 15
            listadoCamas[i].valoresActualesIones[8] = 0
            listadoCamas[i].valoresActualesIones[9] = 70
            listadoCamas[i].valoresActualesIones[10] = 15
            listadoCamas[i].valoresActualesIones[11] = 12
            listadoCamas[i].valoresActualesIones[12] = 200
            listadoCamas[i].valoresActualesIones[13] = 250
            listadoCamas[i].valoresActualesIones[14] = 120
            listadoCamas[i].valoresActualesIones[15] = 232
            listadoCamas[i].valoresActualesIones[19] = 802
            
            reasignarValoresActuales()
            
            marcadorFila = 99 // Se usa un valor que no existe para que no se elimine ninguna variable de asignar índices en el siguiente paso.
            asignarIndices()
            asignarIndicesIones()
        }
            let camasIn = try! JSONEncoder().encode(listadoCamas)
            UserDefaults.standard.set(camasIn, forKey: "ListadoCamas")
           // rellenarCampos()
  
            print("Creación completa")
  
    }
    
    @IBAction func resetTodoButton(_ sender: Any) {
        listadoCamas = []
        creacionValoresBasales(inicio: 0, fin: 24)
        cama = 1
        
        let indice = IndexPath(row: 0, section: 0)
        tabla.selectRow(at: indice, animated: true, scrollPosition: .none)
        marcadorFila = 20 // Se recupera el marcadorFila que se quiere
        tipoPicker = 3 // Picker de cama
        mostrarPicker()
        rellenarCampos()
    }
    
    @IBAction func resetCamaButton(_ sender: Any) {
        listadoCamas.remove(at: Int(cama))
        creacionValoresBasales(inicio: Int(cama), fin: Int(cama))
        cama = 1
        let indice = IndexPath(row: 0, section: 0)
        tabla.selectRow(at: indice, animated: true, scrollPosition: .none)
        marcadorFila = 20 // Se recupera el marcadorFila que se quiere
        tipoPicker = 3 // Picker de cama
        mostrarPicker()
        rellenarCampos()
    }

    
    @IBAction func cerrarVentanaButton(_ sender: UIButton?) {
            if menuShowing {
                alturaConstraint.constant = 124
                pgControlCentralConstraint.constant = 0
                pgControlVeriticalConstraint.constant = -134

            }
            else{
                alturaConstraint.constant = 0
                pgControlCentralConstraint.constant = 145
                pgControlVeriticalConstraint.constant = -10
            }
            menuShowing = !menuShowing
        print("Menushowing despues de boton:\(menuShowing)")
            UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn,animations: {self.view.layoutIfNeeded()})
        }
    func cerrarVentanaAlCargarVistas(){
        if menuShowing {
            alturaConstraint.constant = 0
            pgControlCentralConstraint.constant = 145
            pgControlVeriticalConstraint.constant = -10
        }
        else{
            alturaConstraint.constant = 124
            pgControlCentralConstraint.constant = 0
            pgControlVeriticalConstraint.constant = -134
        }
        //menuShowing = !menuShowing
        
    }
    override func viewWillAppear(_ animated: Bool) {
        cerrarVentanaAlCargarVistas()
        print("Menushowing appear:\(menuShowing)")
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return secciones.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return secciones[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return etiquetas[section].count
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        header.textLabel?.textColor = .blue
        header.textLabel?.textAlignment = NSTextAlignment.center
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // if etiquetas[indexPath.section][indexPath.row] == etiquetaSeleccionada {
        if indexPath == indiceActual {
            switch (indexPath.section, indexPath.row){
                case (0,0):
                    let celda = tableView.dequeueReusableCell(withIdentifier: "MiCeldaCama", for: indexPath) as! CeldaCama
                    celda.valores.text = valores[indexPath.section][indexPath.row]
                    return celda
                default:
                    let celda = tableView.dequeueReusableCell(withIdentifier: "MiCeldaSeleccionada", for: indexPath) as! CeldaSeleccionada
                    celda.etiquetas.text = etiquetas[indexPath.section][indexPath.row]
                    celda.valores.text = valores[indexPath.section][indexPath.row]
                    return celda
            }
        }
        else {
            switch (indexPath.section, indexPath.row){
            case (0,0):
                let celda = tableView.dequeueReusableCell(withIdentifier: "MiCeldaCama", for: indexPath) as! CeldaCama
                celda.valores.text = valores[indexPath.section][indexPath.row]
                return celda
            default:
                let celda = tableView.dequeueReusableCell(withIdentifier: "MiCelda", for: indexPath) as! CeldasView
                celda.etiquetas.text = etiquetas[indexPath.section][indexPath.row]
                celda.valores.text = valores[indexPath.section][indexPath.row]
                return celda
        }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        alturaConstraint.constant = 124
        pgControlCentralConstraint.constant = 0
        pgControlVeriticalConstraint.constant = -134
        menuShowing = false
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseIn,animations: {self.view.layoutIfNeeded()})
        
        picker01.isUserInteractionEnabled = true
        picker01.alpha = 1
        indiceActual = tabla.indexPathForSelectedRow ?? [0,0]
        tabla.reloadData()
        
        switch (indexPath.section, indexPath.row){
        case (0, 0):
            //Número de cama
            tipoPicker = 3
            marcadorFila = 20
            
        case (1, 0):
            //Edad
            tipoPicker = 1
            marcadorFila = 0
            
        case (1, 1):
            //Peso actual
            tipoPicker = 2
            marcadorFila = 1
            
        case (1, 2):
            //Limite Obesidad, posición 17
            marcadorFila = 17
            tipoPicker = 2
            picker01.isUserInteractionEnabled = false
            picker01.alpha = 0.5
            menuShowing = false
            cerrarVentanaButton(nil)
 
            
        case (1, 3):
            //Peso elegido
            tipoPicker = 3
            marcadorFila = 2
            
        case (1, 4):
            //Talla
            tipoPicker = 2
            marcadorFila = 3
            
        case (1, 5):
            //Sexo
            tipoPicker = 3
            marcadorFila = 4
            
        case (1, 6):
            //Calorías basal,posición 18
            marcadorFila = 18
            tipoPicker = 4
            
            picker01.isUserInteractionEnabled = false
            picker01.alpha = 0.5
            menuShowing = false
            cerrarVentanaButton(nil)
            break
            
        case (2, 0):
            //HdeC
            tipoPicker = 5
            marcadorFila = 5
            
        case (2, 1):
            //Lípidos
            tipoPicker = 5
            marcadorFila = 6
            
        case (2, 2):
            //Nitrogeno
            tipoPicker = 1
            marcadorFila = 7
            
        case (3, 0):
            //Calorías totales
            tipoPicker = 4
            marcadorFila = 8
            
        case (3, 1):
            //Calorias/kg
            tipoPicker = 1
            marcadorFila = 9
            
        case (3, 2):
            //GEB
            tipoPicker = 7
            marcadorFila = 10
            
        case (3, 3):
            //Calorías no protéicas
            tipoPicker = 4
            marcadorFila = 11
            
        case (3, 4):
            //Porcentaje HdeC
            tipoPicker = 6
            marcadorFila = 12
            
        case (3, 5):
            //Porcentaje calorías nitrógeno
            tipoPicker = 6
            marcadorFila = 19

            
        case (3, 6):
            //Gramos glucosa/kg
            tipoPicker = 7
            marcadorFila = 13
            
        case (3, 7):
            //Gramos lípidos/kg
            tipoPicker = 7
            marcadorFila = 14
            
        case (4, 0):
            //Cal No Proteicas / gramos N2
            tipoPicker = 5
            marcadorFila = 15
            
        case (4, 1):
            // Gramos proteinas / kg
            tipoPicker = 7
            marcadorFila = 16
            
        default:
            break
        }
        picker01.reloadAllComponents()

        mostrarPicker()
    }
    
    func mostrarPicker(){
        self.view.addSubview(picker01)
        picker01.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.addConstraint(
            NSLayoutConstraint (item: picker01,
            attribute: .width,
            relatedBy: .equal,
            toItem: marcoSliderView,
            attribute: .width,
            multiplier: 0.9,
            constant: 0))
        
        self.view.addConstraint(
            NSLayoutConstraint (item: picker01,
                                attribute: .height,
                                relatedBy: .equal,
                                toItem: marcoSliderView,
                                attribute: .height,
                                multiplier: 0.85,
                                constant: 0))
        
        self.view.addConstraint(
            NSLayoutConstraint (item: picker01,
            attribute: .centerX,
            relatedBy: .equal,
            toItem: marcoSliderView,
            attribute: .centerX,
            multiplier: 1,
            constant: 0))
        
        self.view.addConstraint(
            NSLayoutConstraint (item: picker01,
            attribute: .centerY,
            relatedBy: .equal,
            toItem: marcoSliderView,
            attribute: .centerY,
            multiplier: 1,
            constant: 0))
        
       
        // Situación de los componentes del picker según las posiciones guardadas
        var inicio: Int = 0
        var fin: Int = 0
        switch tipoPicker {
            case 1:
                inicio = 2
                fin = 4
            case 2:
                inicio = 1
                fin = 4
            case 3:
                inicio = 3
                fin = 3
            case 4:
                inicio = 0
                fin = 3
            case 5:
                inicio = 1
                fin = 3
            case 6:
                inicio = 2
                fin = 3
            case 7:
                inicio = 3
                fin = 5
            default:
                break
        }
      for i in inicio...fin {
        picker01.selectRow(listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][i], inComponent: (i - inicio), animated: true)
        }
        
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        switch marcadorFila {
            case 0, 7, 9 :
                return 3
            case 1, 3, 17:
                return 4
            case 2, 4, 20:
                return 1
            case 8, 11, 18:
                return 4
            case 5, 6, 15:
                return 3
            case 12, 19:
                return 2
            default: // 10, 13, 14, 16
                return 3
            }
      
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
 
        switch marcadorFila {
        case 0, 7, 9:
            switch component {
                case 0:
                    return decenas.count
                case 1:
                    return unidades.count
                default:
                    return decimas.count
            }
        case 1, 3, 17:
            switch component {
                case 0:
                    if marcadorFila == 1 || marcadorFila == 3 {
                        return 2
                    }
                    else {
                    return centenas.count
                }
                case 1:
                    return decenas.count
                case 2:
                    return unidades.count
                default:
                    return decimas.count
            }
        case 2, 4, 20:
            switch marcadorFila {
                case 2:
                    return 3
                case 4:
                    return 2
                default:
                    return numeroCama.count
            }
        case 8, 11, 18:
            switch component {
                case 0:
                    return millares.count
                case 1:
                    return centenas.count
                case 2:
                    return decenas.count
                default:
                    return unidades.count
            }
        case 5, 6, 15:
            switch component {
            case 0:
                return centenas.count
            case 1:
                return decenas.count
            default:
                return unidades.count
            }
        case 12, 19:
            switch component {
                case 0:
                    return decenas.count
                default:
                    return unidades.count
            }
        default:
            switch component // 10, 13, 14, 16
            {
            case 0:
                return unidades.count
            case 1:
                return decimas.count
            default:
                return centesimas.count
            }
        }

    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var label = UILabel()
        if let v = view {
            label = v as! UILabel
        }
        label.font = UIFont (name: "Helvetica Neue", size: 21)
        //label.text =  decenas[row]
        label.textAlignment = .center
        label.textColor = UIColor.blue.withAlphaComponent(1)
        
        switch marcadorFila {
        
        case 0:  //Edad
            switch component {
                case 0:
                    label.text =  decenas[row]
                    if listadoCamas[Int(cama)].posicionesGuardadasNutricion[0][2] == 0 {
                        label.textColor = UIColor.blue.withAlphaComponent(0.2)
                    }
            case 1:
                label.text =  unidades[row]
            default:
                label.text =  decimas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasNutricion[0][4] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            }
        case 7:  //Nitrogeno
            switch component {
                case 0:
                    label.text =  decenas[row]
                    if listadoCamas[Int(cama)].posicionesGuardadasNutricion[7][2] == 0 {
                        label.textColor = UIColor.blue.withAlphaComponent(0.2)
                    }
                case 1:
                    label.text =  unidades[row]
                default:
                    label.text =  decimas[row]
                    if listadoCamas[Int(cama)].posicionesGuardadasNutricion[7][4] == 0 {
                        label.textColor = UIColor.blue.withAlphaComponent(0.2)
                    }
                }
        case 9:  //cal/kg
            switch component {
            case 0:
                label.text =  decenas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasNutricion[9][2] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            case 1:
                label.text =  unidades[row]
            default:
                label.text =  decimas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasNutricion[9][4] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            }
        case 1:  //Peso
            switch component {
            case 0:
                label.text =  centenas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasNutricion[1][1] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            case 1:
                label.text =  decenas[row]
            case 2:
                label.text =  unidades[row]
            default:
                label.text =  decimas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasNutricion[1][4] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            }
            
        case 17:  //Limite obesidad
            switch component {
            case 0:
                label.text =  centenas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasNutricion[17][1] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            case 1:
                label.text =  decenas[row]
            case 2:
                label.text =  unidades[row]
            default:
                label.text =  decimas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasNutricion[17][4] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            }
        case 3:  //Talla
            switch component {
            case 0:
                label.text =  centenas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasNutricion[3][1] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            case 1:
                label.text =  decenas[row]
            case 2:
                label.text =  unidades[row]
            default:
                label.text =  decimas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasNutricion[3][4] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            }

        case 2: //Peso elegido
                label.text =  pesos[row]
            
        case 4: //sexo
            label.text =  sexos[row]
            
        case 20: //número cama
            label.text = "Cama " + numeroCama[row]
            
        case 8:  //Calorías totales
            switch component {
                case 0:
                    label.text =  millares[row]
                    if listadoCamas[Int(cama)].posicionesGuardadasNutricion[8][0] == 0 {
                        label.textColor = UIColor.blue.withAlphaComponent(0.2)
                    }
                case 1:
                    label.text =  centenas[row]
                case 2:
                    label.text =  decenas[row]
                default:
                    label.text =  unidades[row]
                }
            
        case 11:  //Calorías no proteicas
            switch component {
                case 0:
                    label.text =  millares[row]
                    if listadoCamas[Int(cama)].posicionesGuardadasNutricion[11][0] == 0 {
                        label.textColor = UIColor.blue.withAlphaComponent(0.2)
                    }
                case 1:
                    label.text =  centenas[row]
                case 2:
                    label.text =  decenas[row]
                default:
                    label.text =  unidades[row]
                }
            
        case 18:  //Calorías totales
            switch component {
                case 0:
                    label.text =  millares[row]
                    if listadoCamas[Int(cama)].posicionesGuardadasNutricion[18][0] == 0 {
                        label.textColor = UIColor.blue.withAlphaComponent(0.2)
                    }
                case 1:
                    label.text =  centenas[row]
                case 2:
                    label.text =  decenas[row]
                default:
                    label.text =  unidades[row]
                }
            
        case 5:  //HdeC
            switch component {
            case 0:
                label.text =  centenas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasNutricion[5][1] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            case 1:
                label.text =  decenas[row]
            default:
                label.text =  unidades[row]
            }
            
        case 6:  //Lípidos
            switch component {
            case 0:
                label.text =  centenas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasNutricion[6][1] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            case 1:
                label.text =  decenas[row]
            default:
                label.text =  unidades[row]
            }
            
        case 15:  //Cal no proteicas / g N2
            switch component {
            case 0:
                label.text =  centenas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasNutricion[15][1] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            case 1:
                label.text =  decenas[row]
            default:
                label.text =  unidades[row]
            }
        
        case 12:  //Porcentaje HdeC
            switch component {
            case 0:
                label.text =  decenas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasNutricion[12][2] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            default:
                label.text =  unidades[row]
            }
            
        case 19:  //Porcentaje calorías nitrógeno
            switch component {
            case 0:
                label.text =  decenas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasNutricion[19][2] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            default:
                label.text =  unidades[row]
            }
       
        case 10:  //GEB
            switch component {
            case 0:
                label.text =  unidades[row]
            case 1:
                label.text =  decimas[row]
                if (listadoCamas[Int(cama)].posicionesGuardadasNutricion[10][4] == 0 && listadoCamas[Int(cama)].posicionesGuardadasNutricion[10][5] == 0) {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            default:
                label.text =  centesimas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasNutricion[10][5] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            }
            
        case 13:  //Gramos glucosa / kg
            switch component {
            case 0:
                label.text =  unidades[row]
            case 1:
                label.text =  decimas[row]
                if (listadoCamas[Int(cama)].posicionesGuardadasNutricion[13][4] == 0 && listadoCamas[Int(cama)].posicionesGuardadasNutricion[13][5] == 0) {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            default:
                label.text =  centesimas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasNutricion[13][5] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            }
            
        case 14:  //Gramos lípidos / kg
            switch component {
            case 0:
                label.text =  unidades[row]
            case 1:
                label.text =  decimas[row]
                if (listadoCamas[Int(cama)].posicionesGuardadasNutricion[14][4] == 0 && listadoCamas[Int(cama)].posicionesGuardadasNutricion[14][5] == 0) {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            default:
                label.text =  centesimas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasNutricion[14][5] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            }
            
        case 16:  //Gramos proteinas / kg
            switch component {
            case 0:
                label.text =  unidades[row]
            case 1:
                label.text =  decimas[row]
                if (listadoCamas[Int(cama)].posicionesGuardadasNutricion[16][4] == 0 && listadoCamas[Int(cama)].posicionesGuardadasNutricion[16][5] == 0) {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            default:
                label.text =  centesimas[row]
                if listadoCamas[Int(cama)].posicionesGuardadasNutricion[16][5] == 0 {
                    label.textColor = UIColor.blue.withAlphaComponent(0.2)
                }
            }
            
        default:
            break
        }
        
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        picker01.reloadAllComponents()

        switch marcadorFila {
        case 0, 7, 9:
            // Número tipo: 99,9
            switch component {
            case 0:
                let indice2 = picker01.selectedRow(inComponent: 0)
                listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][2] = indice2
            case 1:
                let indice3 = picker01.selectedRow(inComponent: 1)
                listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][3] = indice3
            default:
                let indice4 = picker01.selectedRow(inComponent: 2)
                listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][4] = indice4
            }
            
            let valorSumado = Double(decenas[listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][2]])! + Double (unidades[listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][3]])! + Double(decimas[listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][4]])!
            listadoCamas[Int(cama)].valoresActualesNutricion[marcadorFila] = valorSumado
            
        case 1, 3, 17:
            // Número tipo: 999,9
            switch component {
            case 0:
                let indice1 = picker01.selectedRow(inComponent: 0)
                listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][1] = indice1
            case 1:
                let indice2 = picker01.selectedRow(inComponent: 1)
                listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][2] = indice2
            case 2:
                let indice3 = picker01.selectedRow(inComponent: 2)
                listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][3] = indice3
                
            default:
                let indice4 = picker01.selectedRow(inComponent: 3)
                listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][4] = indice4
            }
            let valorSumado = Double(centenas[listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][1]])! + Double (decenas[listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][2]])! + Double(unidades[listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][3]])! + Double(decimas[listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][4]])!
                listadoCamas[Int(cama)].valoresActualesNutricion[marcadorFila] = valorSumado
        case 2, 4:
            // Número tipo: 9
            let indice3 = picker01.selectedRow(inComponent: 0)
            listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][3] = indice3
            listadoCamas[Int(cama)].valoresActualesNutricion[marcadorFila] = Double(unidades[indice3])!
        case 20:
            // Número tipo: 9
            let indice3 = picker01.selectedRow(inComponent: 0)
           
            // Las posiciones de la cama es inamovible, por eso no se cambian en los valores guardados
            //listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][3] = indice3
            //listadoCamas[Int(cama)].valoresActualesNutricion[marcadorFila] = Double(numeroCama[indice3])!
            cama = Double(indice3)
            for i in 0...24 {
                listadoCamas[i].id = Double(indice3)
            }
            let camasIn3 = try! JSONEncoder().encode(listadoCamas)
            UserDefaults.standard.set(camasIn3, forKey: "ListadoCamas")
            
        case 8, 11, 18:
            // Número tipo: 9999
            switch component {
            case 0:
                let indice0 = picker01.selectedRow(inComponent: 0)
                listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][0] = indice0
            case 1:
                let indice1 = picker01.selectedRow(inComponent: 1)
                listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][1] = indice1
            case 2:
                let indice2 = picker01.selectedRow(inComponent: 2)
                listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][2] = indice2
                
            default:
                let indice3 = picker01.selectedRow(inComponent: 3)
                listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][3] = indice3
            }
            let valorSumado = Double(millares[listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][0]])! + Double (centenas[listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][1]])! + Double(decenas[listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][2]])! + Double(unidades[listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][3]])!
            listadoCamas[Int(cama)].valoresActualesNutricion[marcadorFila] = valorSumado

        case 5, 6, 15:
            // Número tipo: 999
            switch component {
            case 0:
                let indice1 = picker01.selectedRow(inComponent: 0)
                listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][1] = indice1
            case 1:
                let indice2 = picker01.selectedRow(inComponent: 1)
                listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][2] = indice2
            default:
                let indice3 = picker01.selectedRow(inComponent: 2)
                listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][3] = indice3
            }
            let valorSumado = Double(centenas[listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][1]])! + Double (decenas[listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][2]])! + Double(unidades[listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][3]])!
            listadoCamas[Int(cama)].valoresActualesNutricion[marcadorFila] = valorSumado

        case 12, 19:
            // Número tipo: 99
            switch component {
            case 0:
                let indice2 = picker01.selectedRow(inComponent: 0)
                listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][2] = indice2
            default:
                let indice3 = picker01.selectedRow(inComponent: 1)
                listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][3] = indice3
            }
            let valorSumado = Double(decenas[listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][2]])! + Double (unidades[listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][3]])!
            listadoCamas[Int(cama)].valoresActualesNutricion[marcadorFila] = valorSumado
            
        default: // 10, 13, 14, 16
            // Número tipo: 9,99
            switch component {
            case 0:
                let indice3 = picker01.selectedRow(inComponent: 0)
                listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][3] = indice3
            case 1:
                let indice4 = picker01.selectedRow(inComponent: 1)
                listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][4] = indice4
            default:
                let indice5 = picker01.selectedRow(inComponent: 2)
                listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][5] = indice5
            }
            let valorSumado = Double(unidades[listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][3]])! + Double (decimas[listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][4]])! + Double(centesimas[listadoCamas[Int(cama)].posicionesGuardadasNutricion[marcadorFila][5]])!
            listadoCamas[Int(cama)].valoresActualesNutricion[marcadorFila] = valorSumado
        }
        print("Cama en esta posición: \(cama)")

        desdoblarvaloresActuales(camaElegida: cama)
    }
    
    func desdoblarvaloresActuales(camaElegida: Double){
        
        // cama = listadoCamas[Int(cama)].valoresActualesNutricion[20]
        
        edad = listadoCamas[Int(cama)].valoresActualesNutricion[0]
        pesoActual = listadoCamas[Int(cama)].valoresActualesNutricion[1]
        limiteObesidad = listadoCamas[Int(cama)].valoresActualesNutricion[17]
        pesoElegido = listadoCamas[Int(cama)].valoresActualesNutricion[2]
        pesoCalculos = listadoCamas[Int(cama)].valoresActualesNutricion[23]
        talla = listadoCamas[Int(cama)].valoresActualesNutricion[3]
        sexo = listadoCamas[Int(cama)].valoresActualesNutricion[4]
        caloriasBasal = listadoCamas[Int(cama)].valoresActualesNutricion[18]
        HdeC = listadoCamas[Int(cama)].valoresActualesNutricion[5]
        lipidos = listadoCamas[Int(cama)].valoresActualesNutricion[6]
        nitrogeno = listadoCamas[Int(cama)].valoresActualesNutricion[7]
        caloriasTotales = listadoCamas[Int(cama)].valoresActualesNutricion[8]
        calKg = listadoCamas[Int(cama)].valoresActualesNutricion[9]
        calHarris = listadoCamas[Int(cama)].valoresActualesNutricion[10]
        caloriasNoProteicas = listadoCamas[Int(cama)].valoresActualesNutricion[11]
        HdeCLipidos = listadoCamas[Int(cama)].valoresActualesNutricion[12]
        gramosGluKg = listadoCamas[Int(cama)].valoresActualesNutricion[13]
        gramosLipidosKg = listadoCamas[Int(cama)].valoresActualesNutricion[14]
        calNPgN2 = listadoCamas[Int(cama)].valoresActualesNutricion[15]
        gramosProtKg = listadoCamas[Int(cama)].valoresActualesNutricion[16]
        calProteinas = listadoCamas[Int(cama)].valoresActualesNutricion[19]
        calGlucosa = listadoCamas[Int(cama)].valoresActualesNutricion[21]
        calLipidos = listadoCamas[Int(cama)].valoresActualesNutricion[22]

        let camasIn = try! JSONEncoder().encode(listadoCamas)
        UserDefaults.standard.set(camasIn, forKey: "listadoCamas")

        
        if marcadorFila != 20 {
            calculoParametros()
        }
        else if marcadorFila == 20 {
            
            //reasignarValoresActuales()
            asignarIndices()
            rellenarCampos()
            
        }

    }

    func calculoHarrisBenedict(peso: Double, talla: Double, edad: Double, sexo: Double) -> Double{
        
        if (pesoCalculos != nil && talla != nil && edad != nil && sexo != nil){
            switch sexo {
            case 0: //Hombre
                caloriasBasal = 66.47 + 13.75 * pesoCalculos + 5 * talla - 6.75 * edad
            case 1: //Mujer
                caloriasBasal = 665.1 + 9.563 * pesoCalculos + 1.85 * talla - 4.676 * edad
            default:
                break
            }
        }
        /*The versions of the HB equation for men and
         women are as follows, with W equal to weight in kilograms,
         H the height in centimeters, and A the age in
         years: 66.47 + (13.75 × W) + (5 × H) – (6.75 × A) for men
         and 665.1 + (9.563 × W) + (1.85 × H) – (4.676 × A) for
         women. The ideal body weight was calculated from
         the height in meters: 22.5 × H2 for men and 21.5 × H2
         for women. Predicted body weight was calculated
         from the height in centimeters: 50 + 0.91 (H – 152.4)
         for men and 45.5 + 0.91 (H – 152.4) for women.
         */
        //caloriasBasal = redondeoCinco(numero: caloriasBasal)
        return caloriasBasal
    }

    func calculoPeso(){

        pesoIdeal = ((talla / 2.54) - 60) * 2.3 + 45.5
        limiteObesidad = pesoIdeal * 1.3
        listadoCamas[Int(cama)].valoresActualesNutricion[17] = limiteObesidad
        pesoAjustado = pesoIdeal + ((pesoActual - pesoIdeal) * 0.25)
        if listadoCamas[Int(cama)].valoresActualesNutricion[2] == 0 {
            pesoCalculos = pesoActual}
        else if listadoCamas[Int(cama)].valoresActualesNutricion[2] == 1 {
            pesoCalculos = pesoIdeal}
        else {
            pesoCalculos = pesoAjustado}

        caloriasBasal = calculoHarrisBenedict(peso: pesoCalculos, talla: talla, edad: edad, sexo: sexo)
        listadoCamas[Int(cama)].valoresActualesNutricion[18] = caloriasBasal
    }
    
    func distribucionCalorias(caloriasTotales: Double){
        calGlucosa = (HdeC * 4 * 100) / caloriasTotales
        calLipidos = (lipidos * 9 * 100) / caloriasTotales
        calProteinas = (nitrogeno * 6.25 * 4 * 100) / caloriasTotales
        listadoCamas[Int(cama)].valoresActualesNutricion[19] = calProteinas
        listadoCamas[Int(cama)].valoresActualesNutricion[20] = calGlucosa
        listadoCamas[Int(cama)].valoresActualesNutricion[21] = calLipidos
    }
 
    func calculoParametros(){
        
       // if  marcadorFila == nil {marcadorFila = 20}
        switch marcadorFila {
            //Cambio de cama
            /*
            case 20:
                print("Se ha elegido cambio cama")
                //cama = numCama
             
             */
            //Edad
            case 0:
                calculoPeso()
                calHarris = caloriasTotales / caloriasBasal

            //Peso Actual
            case 1:
                calculoPeso()
                calHarris = caloriasTotales / caloriasBasal
                calKg = caloriasTotales / pesoCalculos

            //Peso Elegido
            case 2:
                calculoPeso()
                calHarris = caloriasTotales / caloriasBasal
                calKg = caloriasTotales / pesoCalculos
        
            //Talla
            case 3:
                calculoPeso()
                calHarris = caloriasTotales / caloriasBasal
        
             //Sexo
            case 4:
                calculoPeso()
                calHarris = caloriasTotales / caloriasBasal
        
            //Hidratos de Carbono
            case 5:
                caloriasTotales = HdeC * 4 + lipidos * 9 + nitrogeno * 6.25 * 4
                calKg = caloriasTotales / pesoCalculos
                calHarris = caloriasTotales / caloriasBasal
                caloriasNoProteicas = caloriasTotales - (nitrogeno * 4 * 6.25)
                //lipidos = caloriasNoProteicas * (100 - HdeCLipidos) / (9 * 100)
                HdeCLipidos = (HdeC * 4 * 100)  / caloriasNoProteicas
                gramosGluKg = HdeC / pesoCalculos
                gramosLipidosKg = lipidos / pesoCalculos
                calNPgN2 = caloriasNoProteicas / nitrogeno
                distribucionCalorias(caloriasTotales: caloriasTotales)
        
            //Lípidos
            case 6:
                caloriasTotales = HdeC * 4 + lipidos * 9 + nitrogeno * 6.25 * 4
                calKg = caloriasTotales / pesoCalculos
                calHarris = caloriasTotales / caloriasBasal
                caloriasNoProteicas = caloriasTotales - (nitrogeno * 4 * 6.25)
               // HdeC = caloriasNoProteicas * HdeCLipidos / (4 * 100)
                HdeCLipidos = (HdeC * 4 * 100)  / caloriasNoProteicas

                gramosGluKg = HdeC / pesoCalculos
                gramosLipidosKg = lipidos / pesoCalculos
                calNPgN2 = caloriasNoProteicas / nitrogeno
                distribucionCalorias(caloriasTotales: caloriasTotales)
        
            //Nitrógeno
            case 7:
                caloriasTotales = HdeC * 4 + lipidos * 9 + nitrogeno * 6.25 * 4
                calKg = caloriasTotales / pesoCalculos
                calHarris = caloriasTotales / caloriasBasal
                calNPgN2 = caloriasNoProteicas / nitrogeno
                gramosProtKg = nitrogeno * 6.25 / pesoCalculos
                calProteinas = nitrogeno * 6.25 * 4
                distribucionCalorias(caloriasTotales: caloriasTotales)
        
            //Calorías totales
            case 8:
                calKg = caloriasTotales / pesoCalculos
                calHarris = caloriasTotales / caloriasBasal
                caloriasNoProteicas = caloriasTotales - (nitrogeno * 4 * 6.25)
                HdeC = caloriasNoProteicas * HdeCLipidos / (4 * 100)
                lipidos = caloriasNoProteicas * (100 - HdeCLipidos) / (9 * 100)
                gramosGluKg = HdeC / pesoCalculos
                gramosLipidosKg = lipidos / pesoCalculos
                calNPgN2 = caloriasNoProteicas / nitrogeno
                distribucionCalorias(caloriasTotales: caloriasTotales)

            //Calorías / kg
            case 9:
                caloriasTotales = calKg * pesoCalculos
                calHarris = caloriasTotales / caloriasBasal
                caloriasNoProteicas = caloriasTotales - (nitrogeno * 4 * 6.25)
                HdeC = caloriasNoProteicas * HdeCLipidos / (4 * 100)
                lipidos = caloriasNoProteicas * (100 - HdeCLipidos) / (9 * 100)
                gramosGluKg = HdeC / pesoCalculos
                gramosLipidosKg = lipidos / pesoCalculos
                calNPgN2 = caloriasNoProteicas / nitrogeno
                distribucionCalorias(caloriasTotales: caloriasTotales)
        
            //Calorías H-B
            case 10:
                caloriasTotales = calHarris * caloriasBasal
                calKg = caloriasTotales / pesoCalculos
                caloriasNoProteicas = caloriasTotales - (nitrogeno * 4 * 6.25)
                HdeC = caloriasNoProteicas * HdeCLipidos / (4 * 100)
                lipidos = caloriasNoProteicas * (100 - HdeCLipidos) / (9 * 100)
                gramosGluKg = HdeC / pesoCalculos
                gramosLipidosKg = lipidos / pesoCalculos
                calNPgN2 = caloriasNoProteicas / nitrogeno
                distribucionCalorias(caloriasTotales: caloriasTotales)
        
            //Calorías no Proteicas
            case 11:
                caloriasTotales = caloriasNoProteicas + nitrogeno * 4 * 6.25
                calKg = caloriasTotales / pesoCalculos
                calHarris = caloriasTotales / caloriasBasal
                HdeC = caloriasNoProteicas * HdeCLipidos / (4 * 100)
                lipidos = caloriasNoProteicas * (100 - HdeCLipidos) / (9 * 100)
                gramosGluKg = HdeC / pesoCalculos
                gramosLipidosKg = lipidos / pesoCalculos
                calNPgN2 = caloriasNoProteicas / nitrogeno
                distribucionCalorias(caloriasTotales: caloriasTotales)
        
            //HdeC / Lípidos
            case 12:
                HdeC = caloriasNoProteicas * HdeCLipidos / (4 * 100)
                lipidos = caloriasNoProteicas * (100 - HdeCLipidos) / (9 * 100)
                gramosGluKg = HdeC / pesoCalculos
                gramosLipidosKg = lipidos / pesoCalculos
                distribucionCalorias(caloriasTotales: caloriasTotales)
        
            //Gramos Glucosa / kg
            case 13:
                HdeC = gramosGluKg * pesoCalculos
                caloriasTotales = HdeC * 4 + lipidos * 9 + nitrogeno * 6.25 * 4
                calKg = caloriasTotales / pesoCalculos
                calHarris = caloriasTotales / caloriasBasal
                caloriasNoProteicas = caloriasTotales - (nitrogeno * 4 * 6.25)
                calNPgN2 = caloriasNoProteicas / nitrogeno
                distribucionCalorias(caloriasTotales: caloriasTotales)
        
            //Gramos Lípidos / kg
            case 14:
                lipidos = gramosLipidosKg * pesoCalculos
                caloriasTotales = HdeC * 4 + lipidos * 9 + nitrogeno * 6.25 * 4
                calKg = caloriasTotales / pesoCalculos
                calHarris = caloriasTotales / caloriasBasal
                caloriasNoProteicas = caloriasTotales - (nitrogeno * 4 * 6.25)
                calNPgN2 = caloriasNoProteicas / nitrogeno
                distribucionCalorias(caloriasTotales: caloriasTotales)

             //Calorías no protéicas / g de Nitrógeno
            case 15:
                caloriasNoProteicas = calNPgN2 * nitrogeno
                caloriasTotales = caloriasNoProteicas + nitrogeno * 6.25 * 4
                calKg = caloriasTotales / pesoCalculos
                calHarris = caloriasTotales / caloriasBasal
                HdeC = caloriasNoProteicas * HdeCLipidos / (4 * 100)
                lipidos = caloriasNoProteicas * (100 - HdeCLipidos) / (9 * 100)
                gramosGluKg = HdeC / pesoCalculos
                gramosLipidosKg = lipidos / pesoCalculos
                distribucionCalorias(caloriasTotales: caloriasTotales)
        
             //Gramos proteina / kg
            case 16:
                nitrogeno = gramosProtKg * pesoCalculos / 6.25
                caloriasTotales = HdeC * 4 + lipidos * 9 + nitrogeno * 6.25 * 4
                calKg = caloriasTotales / pesoCalculos
                calHarris = caloriasTotales / caloriasBasal
                calNPgN2 = caloriasNoProteicas / nitrogeno
                distribucionCalorias(caloriasTotales: caloriasTotales)
        
            //Porcentaje nitrógeno
            case 19:
                nitrogeno = caloriasTotales * calProteinas / (100 * 4 * 6.25)
                caloriasNoProteicas = caloriasTotales - (nitrogeno * 4 * 6.25)
                HdeC = caloriasNoProteicas * HdeCLipidos / (4 * 100)
                lipidos = caloriasNoProteicas * (100 - HdeCLipidos) / (9 * 100)
                gramosGluKg = HdeC / pesoCalculos
                gramosLipidosKg = lipidos / pesoCalculos
                calNPgN2 = caloriasNoProteicas / nitrogeno
                gramosProtKg = nitrogeno * 6.25 / pesoCalculos
                distribucionCalorias(caloriasTotales: caloriasTotales)

            default:
                break
        }
        reasignarValoresActuales()
        asignarIndices()
        rellenarCampos()
    }
    
    func reasignarValoresActuales(){

        listadoCamas[Int(cama)].valoresActualesNutricion[0] = edad
        listadoCamas[Int(cama)].valoresActualesNutricion[1] = pesoActual
        listadoCamas[Int(cama)].valoresActualesNutricion[17] = limiteObesidad
        listadoCamas[Int(cama)].valoresActualesNutricion[2] = pesoElegido
        listadoCamas[Int(cama)].valoresActualesNutricion[23] = pesoCalculos
        listadoCamas[Int(cama)].valoresActualesNutricion[3] = talla
        listadoCamas[Int(cama)].valoresActualesNutricion[4] = sexo
        listadoCamas[Int(cama)].valoresActualesNutricion[18] = caloriasBasal
        listadoCamas[Int(cama)].valoresActualesNutricion[5] = HdeC
        listadoCamas[Int(cama)].valoresActualesNutricion[6] = lipidos
        listadoCamas[Int(cama)].valoresActualesNutricion[7] = nitrogeno
        listadoCamas[Int(cama)].valoresActualesNutricion[8] = caloriasTotales
        listadoCamas[Int(cama)].valoresActualesNutricion[9] = calKg
        listadoCamas[Int(cama)].valoresActualesNutricion[10] = calHarris
        listadoCamas[Int(cama)].valoresActualesNutricion[11] = caloriasNoProteicas
        listadoCamas[Int(cama)].valoresActualesNutricion[12] = HdeCLipidos
        listadoCamas[Int(cama)].valoresActualesNutricion[13] = gramosGluKg
        listadoCamas[Int(cama)].valoresActualesNutricion[14] = gramosLipidosKg
        listadoCamas[Int(cama)].valoresActualesNutricion[15] = calNPgN2
        listadoCamas[Int(cama)].valoresActualesNutricion[16] = gramosProtKg
        listadoCamas[Int(cama)].valoresActualesNutricion[19] = calProteinas
        listadoCamas[Int(cama)].valoresActualesNutricion[21] = calGlucosa
        listadoCamas[Int(cama)].valoresActualesNutricion[22] = calLipidos
       
        let camasIn3 = try! JSONEncoder().encode(listadoCamas)
        UserDefaults.standard.set(camasIn3, forKey: "ListadoCamas")
    }
    
    func asignarIndices(){
        
        for i in 0...listadoCamas[Int(cama)].valoresActualesNutricion.count - 1 {
            if i != marcadorFila {
                
                let valor: Double = listadoCamas[Int(cama)].valoresActualesNutricion[i]
                if valor != 0 && valor.isFinite{

                    switch i {
                    case 5, 6, 8, 11, 15, 18, 19:
                        let valorReconvertido = Int(round(valor))
                        
                        let indice0 = Int((valorReconvertido / 1000))
                        let resto0 = indice0 * 1000
                        
                        let indice1 = Int(((valorReconvertido - resto0)) / 100)
                        let resto1 = resto0 + indice1 * 100
                        
                        let indice2 = Int(((valorReconvertido - resto1)) / 10)
                        let resto2 = resto1 + indice2 * 10
                        
                        let indice3 = Int((valorReconvertido - resto2))
                        
                        let indice4 = 0
                        let indice5 = 0
                        
                        listadoCamas[Int(cama)].posicionesGuardadasNutricion[i][0] = indice0
                        listadoCamas[Int(cama)].posicionesGuardadasNutricion[i][1] = indice1
                        listadoCamas[Int(cama)].posicionesGuardadasNutricion[i][2] = indice2
                        listadoCamas[Int(cama)].posicionesGuardadasNutricion[i][3] = indice3
                        listadoCamas[Int(cama)].posicionesGuardadasNutricion[i][4] = indice4
                        listadoCamas[Int(cama)].posicionesGuardadasNutricion[i][5] = indice5

                    case 7, 9, 17:
                        let valorReconvertido = Int(round(valor * 10))
                        
                        let indice0 = Int((valorReconvertido / 10000))
                        let resto0 = indice0 * 10000
                        
                        let indice1 = Int(((valorReconvertido - resto0)) / 1000)
                        let resto1 = resto0 + indice1 * 1000
                        
                        let indice2 = Int(((valorReconvertido - resto1)) / 100)
                        let resto2 = resto1 + indice2 * 100
                        
                        let indice3 = Int((valorReconvertido - resto2) / 10)
                        let resto3 = resto2 + indice3 * 10
                        
                        let indice4 = Int((valorReconvertido - resto3))
                        
                        let indice5 = 0
                        listadoCamas[Int(cama)].posicionesGuardadasNutricion[i][0] = indice0
                        listadoCamas[Int(cama)].posicionesGuardadasNutricion[i][1] = indice1
                        listadoCamas[Int(cama)].posicionesGuardadasNutricion[i][2] = indice2
                        listadoCamas[Int(cama)].posicionesGuardadasNutricion[i][3] = indice3
                        listadoCamas[Int(cama)].posicionesGuardadasNutricion[i][4] = indice4
                        listadoCamas[Int(cama)].posicionesGuardadasNutricion[i][5] = indice5
                        
                    case 20:
                        break // La posición del número de cama no se modifica
                    default:
                        let valorReconvertido = Int(round(valor * 100))
                        
                        let indice0 = Int((valorReconvertido / 100000))
                        let resto0 = indice0 * 100000
                        
                        let indice1 = Int(((valorReconvertido - resto0)) / 10000)
                        let resto1 = resto0 + indice1 * 10000
                        
                        let indice2 = Int(((valorReconvertido - resto1)) / 1000)
                        let resto2 = resto1 + indice2 * 1000
                        
                        let indice3 = Int((valorReconvertido - resto2) / 100)
                        let resto3 = resto2 + indice3 * 100
                        
                        let indice4 = Int((valorReconvertido - resto3) / 10)
                        let resto4 = resto3 + indice4 * 10
                        
                        let indice5 = Int((valorReconvertido - resto4))
                        listadoCamas[Int(cama)].posicionesGuardadasNutricion[i][0] = indice0
                        listadoCamas[Int(cama)].posicionesGuardadasNutricion[i][1] = indice1
                        listadoCamas[Int(cama)].posicionesGuardadasNutricion[i][2] = indice2
                        listadoCamas[Int(cama)].posicionesGuardadasNutricion[i][3] = indice3
                        listadoCamas[Int(cama)].posicionesGuardadasNutricion[i][4] = indice4
                        listadoCamas[Int(cama)].posicionesGuardadasNutricion[i][5] = indice5
                    }
                }
            }
        }
    }
    
    func asignarIndicesIones(){
        
        for i in 0...listadoCamas[Int(cama)].valoresActualesIones.count - 1 {
            if i != marcadorFila {
                
                let valor: Double = listadoCamas[Int(cama)].valoresActualesIones[i]
                if valor != 0 && valor.isFinite{
                    
                    switch i {
                    case 0...5, 9: // Numero sin decimales
                        let valorReconvertido = Int(round(valor))
                        
                        let indice0 = Int((valorReconvertido / 1000))
                        let resto0 = indice0 * 1000
                        
                        let indice1 = Int(((valorReconvertido - resto0)) / 100)
                        let resto1 = resto0 + indice1 * 100
                        
                        let indice2 = Int(((valorReconvertido - resto1)) / 10)
                        let resto2 = resto1 + indice2 * 10
                        
                        let indice3 = Int((valorReconvertido - resto2))
                        
                        let indice4 = 0
                        let indice5 = 0
                        
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][0] = indice0
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][1] = indice1
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][2] = indice2
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][3] = indice3
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][4] = indice4
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][5] = indice5
                        
                        
                        
                    case 6, 7, 8, 10, 11: //Número con un decimal
                        let valorReconvertido = Int(round(valor * 10))
                        
                        let indice0 = Int((valorReconvertido / 10000))
                        let resto0 = indice0 * 10000
                        
                        let indice1 = Int(((valorReconvertido - resto0)) / 1000)
                        let resto1 = resto0 + indice1 * 1000
                        
                        let indice2 = Int(((valorReconvertido - resto1)) / 100)
                        let resto2 = resto1 + indice2 * 100
                        
                        let indice3 = Int((valorReconvertido - resto2) / 10)
                        let resto3 = resto2 + indice3 * 10
                        
                        let indice4 = Int((valorReconvertido - resto3))
                        
                        let indice5 = 0
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][0] = indice0
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][1] = indice1
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][2] = indice2
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][3] = indice3
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][4] = indice4
                        listadoCamas[Int(cama)].posicionesGuardadasActualesIones[i][5] = indice5
                        
                        
                    default:
                        break
                    }
                }
            }
        }
        //rellenarCampos()
    }

    func rellenarCampos(){
        
        valores[0][0] = "Cama " + String(format: "%.0f", cama)
        valores[1][0] = String(format: "%.1f", edad)
        valores[1][1] = String(format: "%.1f", pesoActual)
        valores[1][2] = String(format: "%.1f", limiteObesidad)
        valores[1][3] = String(format: "%.1f", pesoCalculos)
            if pesoElegido == 0 {
                etiquetas[1][3] = "Peso elegido: Peso Actual"}
            else if pesoElegido == 1 {
                etiquetas[1][3] = "Peso elegido: Peso Ideal"}
            else {
                etiquetas[1][3] = "Peso elegido: Peso Ajustado"}
        valores[1][4] = String(format: "%.1f", talla)
        if sexo == 0 {
            valores[1][5] = "Hombre"
            }
        else {
            valores[1][5] = "Mujer"
        }
        
        valores[1][6] = String(format: "%.0f", caloriasBasal)
        
        valores[2][0] = String(format: "%.0f", HdeC)
        valores[2][1] = String(format: "%.0f", lipidos)
        valores[2][2] = String(format: "%.1f", nitrogeno)
        
        valores[3][0] = String(format: "%.0f", caloriasTotales)
        valores[3][1] = String(format: "%.1f", calKg)
        valores[3][2] = String(format: "%.2f", calHarris)
        valores[3][3] = String(format: "%.0f", caloriasNoProteicas)
        valores[3][4] = String(format: "%.0f", HdeCLipidos) + " / " + String(format: "%.0f", (100 - HdeCLipidos))
        valores[3][5] = String(format: "%.0f", calGlucosa) + " / " + String(format: "%.0f", calLipidos) + " / " + String(format: "%.0f", calProteinas)
            
        valores[3][6] = String(format: "%.2f", gramosGluKg)
        valores[3][7] = String(format: "%.2f", gramosLipidosKg)


        valores[4][0] = String(format: "%.0f", calNPgN2)
        valores[4][1] = String(format: "%.2f", gramosProtKg)
        
        
        
        tabla.reloadData()
        
        print("Marcador de fila: \(marcadorFila)")
        print("Cama: \(cama)")
        print(listadoCamas[0].id)
        
        /*
        for i in 0...23 {
            print("Cama: \(listadoCamas[i].valoresActualesNutricion[20]). Posición: \(listadoCamas[i].posicionesGuardadasNutricion[20][3])")
        }
 */
    }
}

